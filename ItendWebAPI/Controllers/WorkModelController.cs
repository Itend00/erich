﻿using ItendSolution.Models;
using ItendWebAPI.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItendWebAPI.Controllers
{
    public class WorkModelController : ApiController
    {
        // GET: api/WorkModel
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

    
        [Route("api/Workmodel")]
        [HttpGet]
        // GET: api/User/Workmodel?Nof=<0/1>&ID=<CID/WorkmodelID>
        // if Nof = 0 then ID is Cid ID, Cid = Company ID
        // if Nof = 1 then ID is WorkmodelId
        public List<UserWorkModel> Get(int Nof,int ID)
        {
            List<UserWorkModel> models = null;
            UserDB DB = new UserDB();
            if (Nof==0)
            {
                models = DB.GetWorkModelList(ID); // All company wide workmodels (Cid)
            }
            else 
            {
                models = new List<UserWorkModel>();
                models.Add(DB.GetWorkModel(ID)); // Only workmodel with ID
            }
            return models;
        }
        

        // POST: api/WorkModel
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/WorkModel/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WorkModel/5
        public void Delete(int id)
        {
        }
    }
}
