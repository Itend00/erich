﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Http.Cors;
using System.Web.Cors;
using ItendWebAPI.Areas;
using ItendSolution.Models;
using ItendWebAPI.Database;
using ItendSolution.Shared;


namespace ItendWebAPI.Controllers
{
    // [EnableCors(origins: "", headers: "*", methods: "*")]
    public class CalendarController : ApiController
    {
        /// <summary>
        /// Return CalendarMonth object for a given year, month and language
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="lang">Locale can be EN,FI,CZ, ...  </param>
        /// <returns></returns>
        [Route("api/Calendar")]    // this is default, but ...
        [HttpGet]
        public JsonResult<CalendarMonth> Get(int year, int month, string lang)
        {
            if ((year > 2019) && (year < 2100) && (month > 0) && (month < 13))
            {
                CalendarCalculator cal = new CalendarCalculator(year, month);
                CalendarMonth info = new CalendarMonth();
                RootDB DB = new RootDB();
                info.Year = year;
                info.Month = month;
                info.FirstDay = cal.FirstDay;
                info.NumberOfDays = cal.NofDays;
                DB.CalendarFillInfo(info, lang);
                return Json(info);
            }
            return null;
        }
    }
}
