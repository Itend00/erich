﻿using ItendSolution.Models;
using ItendWebAPI.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web;
using System.Web.Mvc;
using System.Web.Http.Results;

namespace ItendWebAPI.Controllers
{
    // [EnableCors(origins: "", headers: "*", methods: "*")]
    public class LocaleController : ApiController
    {
        
        // POST: api/Locale https://localhost:44354
        public LocaleModel Post(LocaleModel value)
        {
            RootDB DB = new RootDB();
            string retval = DB.GetLocales(value);
            if (retval != null)
            {
               value.Lang = retval;
            }
            return value;
        }
        
    }
}
