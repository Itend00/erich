﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Http.Cors;
using ItendSolution.Models;
using ItendWebAPI.Database;
using ItendSolution.Shared;

namespace ItendWebAPI.Controllers
{
    // [EnableCors(origins: "", headers: "*", methods: "*")]
    public class CountryController : ApiController
    {
        [Route("api/Country/list")]
        [HttpGet]
        // Get: api/Country
        public JsonResult<SupportedCountriesModel> Get()
        {
            RootDB DB = new RootDB();
            return Json(DB.GetCountryList());
        }

        // GET: api/Country/5
        public string Get(int id)
        {
            return "value";
        }

        // PUT: api/Country/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Country/5
        public void Delete(int id)
        {
        }
    }
}
