﻿using System.IO;
using System.Web.Http;
using System.Web.Http.Cors;
using ItendWebAPI.Database;
using ItendSolution.Models;
using ItendSolution.Shared;
using Newtonsoft.Json;

namespace ItendWebAPI.Controllers
{
    // [EnableCors(origins: "", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        [System.Web.Http.Route("api/User")]
        // TODO Check the access rights of a requester in down below.        
        public UserRESTAPI Post([FromBody] UserRESTAPI value)
        {
            UserDB DB = new UserDB();

            value.Success = false; // Pessimist

            switch (value.Action)
            {
                case RestAPIUser.AuthenticateUser:
                    {
                        UserModel User = DB.Authenticate(value.Login, value.Password);
                        // Delete password info, not needed anymore
                        value.Login = "";
                        value.Password = "";
                        value.User = null;
                        if (User != null)
                        {
                            User.Authenticated = true;
                            DB.GetUserInfo(User); // This must not get Password and Login....
                            value.User = User;
                            value.Success = true; // User found.
                            value.User.WorkModel = DB.GetWorkModel(value.User.WorkModel.WorkModelId);
                        }
                    }
                    break;
                case RestAPIUser.AddUser:
                    {
                        DB.AddUser(value.User);
                        value.Success = true;
                    }
                    break;
                case RestAPIUser.GetUser:
                    {
                        value.User = DB.GetUserById(value.Id);
                        value.Success = true;
                    }
                    break;
                case RestAPIUser.UserList:
                    {
                        value.UserList = DB.GetUserList(value.RequestingUser.Cid);
                        value.Success = true;
                    }
                    break;
                case RestAPIUser.WorkmodelList:
                    {
                        value.WorkModelList = DB.GetWorkModelList(value.RequestingUser.Cid);
                        value.Success = true;
                    }
                    break;

                case RestAPIUser.Workmodel:
                    {
                        value.WorkModel = DB.GetWorkModel(value.Id);
                    }
                    break;

                case RestAPIUser.UpdateUser:
                    {
                        value.Success = DB.UpdateUser(value.User);
                    }
                    break;

                case RestAPIUser.MarkForDeletion:
                    {
                        value.Success = DB.MarkForDeletion(value.User);
                    }
                    break;

                case RestAPIUser.SubmitReport:
                    {
                        value.Success = DB.AddReport(value);
                    }
                    break;

                case RestAPIUser.GetReport:
                    {
                        Stream s = DB.GetReport(value);
                        value.BinaryData = new byte[s.Length];
                        s.Position = 0;
                        s.Read(value.BinaryData, 0, (int)s.Length);
                        value.Success = true;
                    }
                    break;

                case RestAPIUser.FetchReport:
                    {
                        value.ReportList = DB.FetchReport(value);
                        value.Success = true;
                    }
                    break;

                case RestAPIUser.ChangePassword:
                    {
                        value.Success = DB.ChangePassword(value);
                    }
                    break;
            }
            return value;
        }
        

        // PUT: api/User/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
        }
    }
}
