﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

using ItendWebAPI.Communication; // Email testing

/*
 * Test controller, can be used and edited as you wish.
 * 
 * Idea is to test some functionality that is not integrated to anywhere.
 * I made this to develop an email class for sending emails.
 * 
 * see public string Get(string strFun). Add your systems there.
 * 
 * Call -> localhost/test?fun=kakapopo
 * or in debug -> https://localhost:44395/test?fun=kakapopo
 */

namespace ItendWebAPI.Controllers
{
    public class TestController : ApiController
    {
        [System.Web.Http.Route("test")]
        public string Get(string fun)
        {
            string retVal = "Undefined request";
            if (fun.Equals("test"))
                return "Still alive and kicking";

            if (fun.Equals("email"))
            {
                MyEmail email = new MyEmail("itend@mail.com", "mrekkila@gmail.com", "Testing ITEND email");
                email.SendMessage("Kukkuu");
                retVal = "Mail Sent";
            }
            return retVal;
        }


        // POST: api/Test
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Test/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE: api/Test/5
        //public void Delete(int id)
        //{
        //}
    }
}
