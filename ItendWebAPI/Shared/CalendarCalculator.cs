﻿
namespace ItendSolution.Shared
{
    public class CalendarCalculator
    {
        private static readonly int[] _daylist = new[]
        {
            6, 2, 1, 4, 6, 2, 4, 0, 3, 5, 1, 3
        };

        public int NofDays;
        public int FirstDay;

        /// <summary>
        /// Calculates the first day of the month [0..6] "=[Mon..Sun]" and number of days in that month.
        /// Algorithm: Tomohiko Sakamoto...
        /// </summary>
        /// <param name="y">Year</param>
        /// <param name="m">Month</param>
        public CalendarCalculator(int y, int m)
        {
            this.NofDays = _NumberOfDays(y, m);
            this.FirstDay = _FirstDay(y, m);
        }


        private static int _FirstDay(int y,int m)
        {
            int tmy;

            /* first day of the month  0..6*/
            tmy = y - ((m < 3) ? 1 : 0);
            return  (tmy + tmy / 4 - tmy / 100 + tmy / 400 + _daylist[m - 1] + 1) % 7;
        }
        private int _NumberOfDays(int y, int m)
        {
            int retval;
            bool leapYear;

            retval = 31;
            if (m == 2)
            {
                retval= 28;
                leapYear = (y % 4 == 0 && y % 100 != 0) || (y % 400 == 0);
                if (leapYear)
                {
                    retval = 29;
                }
            }
            else
            {
                if (m == 4 || m == 6 || m == 9 || m == 11)
                    retval = 30;
            }
            return retval;
        }
        
        /// <summary>
        /// Calculate what day of week is this date.
        /// 0 = Monday 6= Sunday
        /// </summary>
        /// <param name="y">Year</param>
        /// <param name="m">Month</param>
        /// <param name="d">Day</param>
        /// <returns>Number of weekday starting from Monday = 0</returns>
        public static int WeekDay(int y, int m, int d)
        {
            int fday = _FirstDay(y, m);
            return (fday + d - 1) % 7;
        }
    }
}