﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ItendSolution.Shared
{
    /// <summary>
    /// These are used in both server and client.
    /// So, If you make changes both needs to be updated.
    /// Also remember that these values go also to the Database
    /// </summary>
    public static class RestAPIUser
    {
        public const int Undefined = -1;
        public const int AuthenticateUser = 0;
        public const int GetUser = 1; // Those who think about ENUMing this -> Forget it.
        public const int AddUser = 2;
        public const int UserList = 3;
        public const int UpdateUser = 4;
        public const int WorkmodelList = 5;
        public const int Workmodel = 6;
        public const int MarkForDeletion = 7;
        public const int SubmitReport = 8;
        public const int GetReport = 9;  // For accounting and HR
        public const int FetchReport = 10; // For calendar to display the report
        public const int ExistingUser = 11; // for first login
        public const int ChangePassword = 12;
    }

    // RestAPIReport.Undefined
    // RestAPIReport.Worktime
    // RestAPIReport.UnpaidOff
    // RestAPIReport.PaidOff
    // RestAPIReport.SickDay
    // RestAPIReport.Sickness
    // RestAPIReport.Holiday
    // RestAPIReport.Lunch
    // RestAPIReport.Overtime
    // RestAPIReport.Oncall

    public static class RestAPIReport
    {
        public const int Undefined = -1;
        public const int Worktime = 0;
        public const int UnpaidOff = 1;
        public const int PaidOff = 2;
        public const int SickDay = 3;
        public const int Sickness = 4;
        public const int Holiday = 5;
        public const int Lunch = 6;
        public const int Overtime = 7;
        public const int Oncall = 8;
        public const int EndOfDef = 9; // Change this if you put more here

        public static readonly string[] _name = {
        "label_day_type_undefined",
        "label_day_type_worktime",
        "label_day_type_unpaid_off",
        "label_day_type_paid_off",
        "label_day_type_sickday",
        "label_day_type_sickness",
        "label_day_type_holiday",
        "label_day_type_lunch",
        "label_day_type_overtime",
        "label_day_type_oncall",
        null,
        };

        public static string Name(int ident) => _name[ident + 1];

        public static List<string> NameList()
        {
            List<string> retval = new List<string>();
            for( int i=0; _name[i]!=null; i++)
            {
                retval.Add(_name[i]);
            }
            return retval;
        }
    }
}
