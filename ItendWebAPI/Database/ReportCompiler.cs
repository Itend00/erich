﻿using ItendSolution.Models;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ItendWebAPI.Areas;
using ItendSolution.Shared;
using System;

namespace ItendWebAPI.Database
{
    public class ReportCompiler
    {
        private ExcelWriter Excel;
        private class ReportFetcher
        {
            private DBSession MySession;
            private SqlDataReader Reader;
            public ReportFetcher()
            {
                MySession = new DBSession(DBSession.MyDatabase.User);
            }

            public void Close()
            {
                MySession.Close();
            }

            public void Open(int cid, int year, int month)
            {
                string query = "SELECT Object FROM dbo.MonthlyReports WHERE (Cid=@cid AND Year=@year AND Month=@month) ORDER BY Id";
                SqlCommand comm = new SqlCommand(query)
                {
                    Connection = MySession.Connection
                };

                comm.Parameters.Add("@cid", SqlDbType.Int);
                comm.Parameters.Add("@year", SqlDbType.Int);
                comm.Parameters.Add("@month", SqlDbType.Int);

                comm.Parameters["@cid"].Value = cid;
                comm.Parameters["@year"].Value = year;
                comm.Parameters["@month"].Value = month;
                this.Reader = comm.ExecuteReader();
            }

            public string Read()
            {
                string retval=null;
                if (Reader.Read())
                {
                    retval = Reader.GetString(Reader.GetOrdinal("Object"));
                }
                return retval;
            }

            public void End()
            {
                Reader.Close();
                Reader = null;
            }
        }

                
        public ReportCompiler()
        {
            Excel = new ExcelWriter("ExcelReportTemplate.xlsx");
        }

        private void TranslateSheet(UserRESTAPI Request)
        {
            int row, col, ind;
            string cell,value;
            // Debug / development
            LocaleModel Loc = new LocaleModel(Request.RequestingUser.Locale);
            Excel.SelectSheet("excel_sheet_translations");
            cell = Excel.GetNamedAddress("Translations");
            row = ind = Excel.GetNamedRow(cell);
            col = Excel.GetNamedCol(cell);

            value = Excel.Doc.GetCellValueAsString(ind++, col);
            while(value.Length>2)
            {
                Loc.AddLocale(value);
                value = Excel.Doc.GetCellValueAsString(ind++, col);
            }
            RootDB rDB = new RootDB();
            rDB.GetLocales(Loc);

            for( ind = 0; ind<Loc.Translation.Count; ind++ )
            {
                Excel.SetCellValue(row + ind, col + 1, Loc.Translation[ind]);
            }

            // Fix the month locale
            Loc.Clear();
            cell = Excel.GetNamedAddress("excel_locale_month");
            value = "month" + Request.ReportList.PeriodStart.Month.ToString();
            row = Excel.GetNamedRow(cell);
            col = Excel.GetNamedCol(cell);
            Loc.AddLocale(value);
            rDB.GetLocales(Loc);
            Excel.Doc.SetCellValue(row, col, Loc.Translation[0]);

            // Year
            cell = Excel.GetNamedAddress("excel_year");
            row = Excel.GetNamedRow(cell);
            col = Excel.GetNamedCol(cell);
            Excel.Doc.SetCellValue(row, col, Request.ReportList.PeriodStart.Year.ToString());

            // Month
            cell = Excel.GetNamedAddress("excel_month");
            row = Excel.GetNamedRow(cell);
            col = Excel.GetNamedCol(cell);
            Excel.Doc.SetCellValue(row, col, Request.ReportList.PeriodStart.Month.ToString());
        }

        private int _Max(int[] arr, int nof)
        {
            int retval = arr[0];
            for (int i=1; i<nof; i++)
            {
                retval = Math.Max(arr[i], retval);
            }
            return retval;
        }
        public Stream GenerateReport(ReportContainer cont, UserRESTAPI Request)
        {
            List<int> ListUid = cont.ListUid();
            int i, d, r, row;
            int[] delta = new int[RestAPIReport.EndOfDef];
            float[] hours = new float[RestAPIReport.EndOfDef];
            float saturdayWork, sundayWork;
            int col, dr, dc;
            int summary_row, summary_col;
            string addr;
            UserDB DB = new UserDB();
            UserModel User;
            ReportModel Rep;

            Excel.Doc.SelectWorksheet("excel_sheet_summary");
            addr = Excel.GetNamedAddress("excel_summary_start");
            summary_row = Excel.GetNamedRow(addr) + 1;
            summary_col = Excel.GetNamedCol(addr);

            for (i = 0; i < ListUid.Count; i++)
            {
                User = DB.GetUserById(ListUid[i]);
                Excel.Doc.CopyWorksheet("excel_sheet_detailed",User.Alias);
                Excel.Doc.SelectWorksheet(User.Alias);

                addr = Excel.GetNamedAddress("excel_date");
                dr = Excel.GetNamedRow(addr)+1; // Excel format, Check from the excel template.
                dc = Excel.GetNamedCol(addr);

                addr = Excel.GetNamedAddress("excel_normal_work");
                row = Excel.GetNamedRow(addr);
                row += 2; // Excel format, Check from the excel template.

                for (int j = 0; j < RestAPIReport.EndOfDef; j++)
                {
                    hours[j] = 0.0f;
                }
                saturdayWork = sundayWork = 0.0f;

                for (d = 1; d < 32; d++)
                {
                    for (int j = 0; j < RestAPIReport.EndOfDef; j++)
                    {
                        delta[j] = 0;
                    }
                    Rep = cont.GetDateFirstEntry(d, ListUid[i]);
                    if (Rep != null)
                    {
                        Excel.SetCellValue(dr, dc, d.ToString());
                    }
                    while (Rep!=null)
                    {
                        switch (Rep.Type)
                        {
                            case RestAPIReport.Worktime:
                                addr = Excel.GetNamedAddress("excel_normal_work");
                                col = Excel.GetNamedCol(addr);
                                r = row + (delta[RestAPIReport.Worktime]++);
                                Excel.SetCellValue(r, col, Rep.Start.MyTimeGet(":"));
                                Excel.SetCellValue(r, col+1, Rep.End.MyTimeGet(":"));                                
                                switch(CalendarCalculator.WeekDay(Rep.Start.Year, Rep.Start.Month, Rep.Start.Day))
                                {
                                    case 5: /* Saturday */
                                        saturdayWork += Rep.DeltaTime;
                                        break;
                                    case 6: /* Sunday */
                                        sundayWork += Rep.DeltaTime;
                                        break;

                                    default:
                                        hours[RestAPIReport.Worktime] += Rep.DeltaTime;
                                        break;
                                }
                                break;

                            case RestAPIReport.Lunch:
                                addr = Excel.GetNamedAddress("excel_lunch_break");
                                col = Excel.GetNamedCol(addr);
                                r = row + (delta[RestAPIReport.Lunch]++);
                                Excel.SetCellValue(r, col, Rep.Start.MyTimeGet(":"));
                                Excel.SetCellValue(r, col + 1, Rep.End.MyTimeGet(":"));
                                hours[RestAPIReport.Lunch] += Rep.DeltaTime;
                                break;

                            case RestAPIReport.Overtime:
                                addr = Excel.GetNamedAddress("excel_overtime");
                                col = Excel.GetNamedCol(addr);
                                r = row + (delta[RestAPIReport.Overtime]++);
                                Excel.SetCellValue(r, col, Rep.Start.MyTimeGet(":"));
                                Excel.SetCellValue(r, col + 1, Rep.End.MyTimeGet(":"));
                                hours[RestAPIReport.Overtime] += Rep.DeltaTime;
                                break;

                            case RestAPIReport.Oncall:
                                addr = Excel.GetNamedAddress("excel_oncall");
                                col = Excel.GetNamedCol(addr);
                                r = row + (delta[RestAPIReport.Oncall]++);
                                Excel.SetCellValue(r, col, Rep.Start.MyTimeGet(":"));
                                Excel.SetCellValue(r, col + 1, Rep.End.MyTimeGet(":"));
                                hours[RestAPIReport.Oncall] += Rep.DeltaTime;
                                break;

                            case RestAPIReport.Holiday:
                                addr = Excel.GetNamedAddress("excel_vacation");
                                col = Excel.GetNamedCol(addr);
                                r = row + (delta[RestAPIReport.Holiday]++);
                                Excel.SetCellValue(r, col, Rep.StrDeltaTime);
                                hours[RestAPIReport.Holiday] += Rep.DeltaTime;
                                break;

                            case RestAPIReport.Sickness:
                                addr = Excel.GetNamedAddress("excel_sickness");
                                col = Excel.GetNamedCol(addr);
                                r = row + (delta[RestAPIReport.Sickness]++);
                                Excel.SetCellValue(r, col, Rep.StrDeltaTime);
                                hours[RestAPIReport.Sickness] += Rep.DeltaTime;
                                break;

                            case RestAPIReport.SickDay:
                            case RestAPIReport.PaidOff:
                                addr = Excel.GetNamedAddress("excel_paid");
                                col = Excel.GetNamedCol(addr);
                                r = row + (delta[RestAPIReport.PaidOff]++);
                                Excel.SetCellValue(r, col, Rep.StrDeltaTime);
                                hours[RestAPIReport.SickDay] += Rep.DeltaTime;
                                hours[RestAPIReport.PaidOff] += Rep.DeltaTime;
                                break;

                            case RestAPIReport.UnpaidOff:
                                addr = Excel.GetNamedAddress("excel_unpaid");
                                col = Excel.GetNamedCol(addr);
                                r = row + (delta[RestAPIReport.UnpaidOff]++);
                                Excel.SetCellValue(r, col, Rep.StrDeltaTime);
                                hours[RestAPIReport.UnpaidOff] += Rep.DeltaTime;
                                break;

                            default:
                                break;
                        }
                        Rep = cont.GetDateNextEntry(d, ListUid[i]);
                        if (Rep==null) // Make the excel a little easier to read by adding underline.
                        {
                            int start, end;
                            dr=row + _Max(delta, RestAPIReport.EndOfDef);
                            addr = Excel.GetNamedAddress("excel_board_end");
                            end = Excel.GetNamedCol(addr);
                            addr = Excel.GetNamedAddress("excel_date");
                            start = Excel.GetNamedCol(addr);
                            SpreadsheetLight.SLStyle CellStyle;
                            for (int ind = start; ind <= end; ind++)
                            {
                                CellStyle = Excel.Doc.GetCellStyle(dr - 1, ind);
                                CellStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Medium, SpreadsheetLight.SLThemeColorIndexValues.Dark1Color);
                                Excel.Doc.SetCellStyle(dr-1, ind, CellStyle);
                            }
                        }
                    }
                    row += _Max(delta, RestAPIReport.EndOfDef);
                }
                addr = Excel.GetNamedAddress("excel_employee_id");
                dr = Excel.GetNamedRow(addr); // Excel format, Check from the excel template.
                dc = Excel.GetNamedCol(addr);
                Excel.SetCellValue(dr, dc + 1, User.CompanyId);


                addr = Excel.GetNamedAddress("excel_alias");
                dr = Excel.GetNamedRow(addr); // Excel format, Check from the excel template.
                dc = Excel.GetNamedCol(addr);
                Excel.SetCellValue(dr, dc + 1, User.Alias);

                addr = Excel.GetNamedAddress("excel_email");
                dr = Excel.GetNamedRow(addr); // Excel format, Check from the excel template.
                dc = Excel.GetNamedCol(addr);
                Excel.SetCellValue(dr, dc + 1, User.Email);
                _ = Excel.Doc.DeleteRow(row, 200);

                Excel.SelectSheet("excel_sheet_summary");
                Excel.SetCellValue(summary_row, summary_col, User.CompanyId);
                Excel.SetCellValue(summary_row, summary_col + 1, User.Alias);
                Excel.SetCellValue(summary_row, summary_col + 2, User.Email);
                Excel.SetCellValue(summary_row, summary_col + 3, hours[RestAPIReport.Worktime]);
                if (saturdayWork > 0.0001) Excel.SetCellValue(summary_row, summary_col + 4, saturdayWork);
                if (sundayWork > 0.0001) Excel.SetCellValue(summary_row, summary_col + 5, sundayWork);
                Excel.SetCellValue(summary_row, summary_col + 7, hours[RestAPIReport.Overtime]);
                Excel.SetCellValue(summary_row, summary_col + 8, hours[RestAPIReport.Oncall]);

                summary_row++;
            }            
            this.TranslateSheet(Request);
            Excel.Doc.HideWorksheet("excel_sheet_detailed");
            Stream s = Excel.WriteToStream();
            s.Position = 0;
            return s;
        }
    }
}
