﻿using ItendSolution.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Remoting.Messaging;

/*
 * Template
 * 
public static string XXXX(XXX)
{
    string retval = null;
    SqlCommand command = null;
    SqlDataReader dataReader = null;

    try
    {
        DBRootConnect();

        string query = "SELECT " + XXX + " FROM dbo.LocaleCalendar WHERE " + XXX;
        command = new SqlCommand(query);
        command.Connection = _RootConnection;
        dataReader = command.ExecuteReader();
        
        dataReader.Read();
        if (dataReader.HasRows)
        {
            //TODO: Insert code here.
        }

        dataReader.Close();dataReader=null;
        command.Dispose();command=null;
    }
    catch (Exception e)
    {
        if (dataReader!=null) dataReader.Close();
        if (command!=null) command.Dispose();
        retval = "DB Fail: " + XXX + e.Message;
    }
    return retval;
}*/

namespace ItendWebAPI.Database
{
    public class RootDB
    {
        private DBSession MySession = null;

        public RootDB()
        {
            MySession = new DBSession(DBSession.MyDatabase.Root);
        }

        /// <summary>
        /// Get the localized calendar related strings form the localisation database for user
        /// These are holidays, months, days,...
        /// </summary>
        /// <param name="country">Country Short Name ex. EN, FI, CZ, CA etc...</param>
        /// <param name="locale">Locale ex. Monday, January etc... </param>
        /// <param name="blong">Return long(true) or short(false) versions of the locale</param>
        public string CalendarLocaleGetShort(string country, string locale, bool blong)
        {
            SqlCommand command = null;
            SqlDataReader dataReader = null;

            string retval = null;
            try
            {
                string ext = "_S"; // Defined in the database that the colum for example english is EN for long and EN_S for short
                if (blong) ext = "";

                string query = "SELECT " + country + ext + " FROM dbo.LocaleCalendar WHERE Locale=\'" + locale + "\'";
                command = new SqlCommand(query);
                command.Connection = MySession.Connection;
                dataReader = command.ExecuteReader(CommandBehavior.SingleRow);
                dataReader.Read();
                if (dataReader.HasRows)
                {
                    retval = dataReader.GetString(0);
                }
            }
            catch (Exception e)
            {
                retval = "DB Fail: " + country + "->" + locale + ":" + e.Message;
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }

        /// <summary>
        /// Get the localized week day names from the localisation database for user
        /// These are holidays, months, days,...
        /// </summary>
        /// <param name="language">Country Short Name ex. EN, FI, CZ, CA etc...</param>
        /// <param name="blong">Return long(true) or short(false) versions of the locale</param>
        public List<string> CalendarLocaleWeekdays(string language, bool blong)
        {
            SqlCommand command = null;
            SqlDataReader dataReader = null;

            List<string> retval = new List<string>(13);
            try
            {
                string ext = "_S"; // Defined in the database that the colum is EN for long and EN_S for short
                if (blong) ext = "";

                string str = "IN (\'Monday\',\'Tuesday\',\'Wednesday\',\'Thursday\',\'Friday\',\'Saturday\',\'Sunday\')";
                string query = "SELECT " + language + ext + " FROM dbo.LocaleCalendar WHERE Locale " + str;
                command = new SqlCommand(query);
                command.Connection = MySession.Connection; 
                dataReader = command.ExecuteReader();

                int day;
                for (day = 0; day < 7; day++) //Just to ensure that we read only from Monday to Sunday.
                {
                    dataReader.Read();
                    if (dataReader.HasRows)
                    {
                        retval.Add(dataReader.GetString(0));
                    }
                }
            }
            catch (Exception e)
            {
                retval.Add("DB Fail: CalendarLocaleWeekdays " + language + ":" + e.Message);
                int i;
                for (i = 0; i < 6; i++)
                {
                    retval.Add("DB Fail");
                }
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }

        /// <summary>
        /// Get the localized month names from the localisation database for user
        /// Returns a list of strings indexed from 1..12, 0=Dooms-ary
        /// </summary>
        /// <param name="country">Country Short Name ex. EN, FI, CZ, CA etc...</param>
        /// <param name="blong">Return long(true) or short(false) versions of the locale</param>
        public List<string> CalendarLocaleMonths(string country, bool blong)
        {
            SqlCommand command = null;
            SqlDataReader dataReader = null;

            List<string> retval = new List<string>(13); // Numbering starts from 1=January, 12 = December

            try
            {
                string ext = "_S"; // Defined in the database that the colum is EN for long and EN_S for short
                if (blong) ext = "";

                string locale = "IN (\'January\',\'February\',\'March\',\'April\',\'May\',\'June\',\'July\',\'August\',\'September\',\'October\',\'November\',\'December\')";
                string query = "SELECT " + country + ext + " FROM dbo.LocaleCalendar WHERE Locale " + locale;
                command = new SqlCommand(query);
                command.Connection = MySession.Connection;
                dataReader = command.ExecuteReader();

                int month;
                retval.Add("Dooms-ary");
                for (month = 0; month < 12; month++) //Just to ensure that we read only from January to December.
                {
                    dataReader.Read();
                    retval.Add(dataReader.GetString(0));
                }
            }
            catch (Exception e)
            {
                retval.Add("DB Fail: CalendarLocaleWeekdays " + country + ":" + e.Message);
                int i;
                for (i = 0; i < 12; i++)
                {
                    retval.Add("DB Fail");
                }
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }

        /// <summary>
        /// Fills the CalendarMonth object with the localized data.
        /// </summary>
        /// <param name="info"> CalendarMonth object. Must contain first day & month as input </param>
        /// <param name="language">Language to use Name ex. EN, FI, CZ, CA etc...</param>
        public string CalendarFillInfo(CalendarMonth info, string language)
        {
            string retval = null;
            SqlCommand command = null;
            SqlDataReader dataReader = null;

            try
            {
                string llong = language;
                string lshort = language + "_S";

                List<string> locales = new List<string> { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

                string query = "SELECT " + llong + "," + lshort + " FROM dbo.LocaleCalendar WHERE Locale =" + "\'" + locales[info.Month - 1] + "\'";
                command = new SqlCommand(query);
                command.Connection = MySession.Connection;
                dataReader = command.ExecuteReader();

                dataReader.Read();
                info.MonthLong = dataReader.GetString(dataReader.GetOrdinal(llong));
                info.MonthShort = dataReader.GetString(dataReader.GetOrdinal(lshort));

                dataReader.Close(); dataReader = null;
                command.Dispose(); command = null;

                // Here we assume that the DB returns weekdays in order mon,tue,wed....
                string unionstr = "(\'Monday\',\'Tuesday\',\'Wednesday\',\'Thursday\',\'Friday\',\'Saturday\',\'Sunday\')";
                query = "SELECT " + " " + llong + "," + lshort + " FROM dbo.LocaleCalendar WHERE Locale IN " + unionstr;
                command = new SqlCommand(query);
                command.Connection = MySession.Connection;
                dataReader = command.ExecuteReader();

                for (int i = 0; i < 7; i++)
                {
                    dataReader.Read();
                    info.DayNamesLong[i] = dataReader.GetString(dataReader.GetOrdinal(llong));
                    info.DayNamesShort[i] = dataReader.GetString(dataReader.GetOrdinal(lshort));
                }
            }
            catch (Exception e)
            {
                retval = "DB Fail: CalendarFillInfo " + language + ":" + e.Message;
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }
        public string GetLocales(LocaleModel L)
        {
            string retval = null;

            string query = "";
            SqlCommand command = null;
            SqlDataReader dataReader = null;

            string strLocale, strTranslation;

            try
            {
                string str = "(";
                for (int i = 0; i < L.Locale.Count; i++)
                {
                    str = str + "\'" + L.Locale[i] + "\'" + ((i < L.Locale.Count - 1) ? "," : "");
                }
                str = str + ")";
                query = "SELECT Locale," + L.Lang + " FROM dbo.Locale WHERE Locale IN " + str;
                command = new SqlCommand(query);
                command.Connection = MySession.Connection;
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    strLocale = dataReader.GetString(dataReader.GetOrdinal("Locale"));
                    strTranslation = dataReader.GetString(dataReader.GetOrdinal(L.Lang));
                    L.AddTranslated(strLocale, strTranslation);
                }
            }
            catch (Exception e)
            {
                retval = "DB Fail: GetLocales >" + L.Lang + "<" + query + "-" + e.Message;
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }

        public SupportedCountriesModel GetCountryList()
        {
            SqlCommand command = null;
            SqlDataReader dataReader = null;

            SupportedCountriesModel retval = new SupportedCountriesModel();

            try
            {
                string query = "SELECT * FROM dbo.LocaleList";
                command = new SqlCommand(query);
                command.Connection = MySession.Connection;
                dataReader = command.ExecuteReader();
                int Column;

                while (dataReader.Read())
                {
                    Column = dataReader.GetOrdinal("Locale");
                    retval.Locale.Add(dataReader.IsDBNull(Column) ? "" : dataReader.GetString(Column).Trim());

                    Column = dataReader.GetOrdinal("Language");
                    retval.Language.Add(dataReader.IsDBNull(Column) ? "" : dataReader.GetString(Column).Trim());

                    Column = dataReader.GetOrdinal("Country");
                    retval.Country.Add(dataReader.IsDBNull(Column) ? "" : dataReader.GetString(Column).Trim());
                }
            }
            catch (Exception e)
            {
                // TODO add logger
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }
    }
}
