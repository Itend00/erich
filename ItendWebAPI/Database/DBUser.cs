﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ItendSolution.Models;

namespace ItendWebAPI.Database
{
    public static class UserServer
    {
        
        private static readonly string _ConnectionString = "Data Source=DESKTOP-413NLCO;Initial Catalog=ITendActual;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private static SqlConnection _Connection = null;

        public static string Connect()
        {
            string retval = null;
            if (_Connection == null)
            {
                try
                {
                    _Connection = new SqlConnection(_ConnectionString);
                    _Connection.Open();
                }
                catch (Exception e)
                {
                    retval = "User database open :"+e.Message;
                }
            }
            return retval;
        }

        public static void Disconnect()
        {
            if (_Connection != null)
            {
                _Connection.Close();
            }
        }

        public static UserModel GetUser(string UniqueIdentifier)
        {
            //
            // TODO User id needs to be dynamic. Look below sql query
            //
            UserModel retval = new UserModel();
            try
            {
                SqlCommand command;
                SqlDataReader dataReader;

                    
                string query = "SELECT * FROM dbo.User WHERE UserId\'6f9619ff-8b86-d011-b42d-00c04fc964ff\'";
                command = new SqlCommand(query, _Connection);
                dataReader = command.ExecuteReader(CommandBehavior.SingleRow);

                dataReader.Read();

                retval.Alias = dataReader.GetString(dataReader.GetOrdinal("Alias"));
                retval.SessionToken = dataReader.GetString(dataReader.GetOrdinal("UserId")); // For now let's use user id for this.
                retval.IsAdmin = (dataReader.GetInt32(dataReader.GetOrdinal("IsAdmin")) != 0);
                retval.IsAccounting = (dataReader.GetInt32(dataReader.GetOrdinal("IsAccountin")) != 0);
                retval.IsUser = (dataReader.GetInt32(dataReader.GetOrdinal("IsUser")) != 0);
                retval.ExtentedPermissions = (dataReader.GetInt32(dataReader.GetOrdinal("ExtendedPermissions")) != 0);
                retval.Vacation = dataReader.GetInt32(dataReader.GetOrdinal("Vacation"));
                retval.Sickdays = dataReader.GetInt32(dataReader.GetOrdinal("Sickdays"));

                dataReader.Close();
                command.Dispose();
            }
            catch (Exception e)
            {
                retval.Alias = "DB User: GetUser() "+"->"+ UniqueIdentifier + ":" + e.Message;
            }
            return retval;
        }
    }
}