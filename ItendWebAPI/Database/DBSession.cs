﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ItendWebAPI.Database
{
    public class DBSession
    {
        public enum MyDatabase { Root, User };
        /*
         *Data Source=dayontap00.database.windows.net;Initial Catalog=ITend;Persist Security Info=True;User ID=itend;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False
         */
        // private readonly string RootConnectionString = "Data Source=DESKTOP-413NLCO;Initial Catalog=ITend;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //private readonly string UserConnectionString = "Data Source=DESKTOP-413NLCO;Initial Catalog=ITendActual;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private readonly string RootConnectionString = "Data Source=dayontap00.database.windows.net;Initial Catalog=ITend;Persist Security Info=True;User ID=itend;Password=a4c@5bPP[9;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False";
        private readonly string UserConnectionString = "Data Source=dayontap00.database.windows.net;Initial Catalog=ITendActual;Persist Security Info=True;User ID=itend;Password=a4c@5bPP[9;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False";

        public SqlConnection Connection;

        public DBSession(MyDatabase database)
        {
            Connection = null;
            try
            {
                switch(database)
                {
                    case MyDatabase.Root:
                        {
                            Connection = new SqlConnection(RootConnectionString);
                        }
                        break;
                    case MyDatabase.User:
                        {
                            Connection = new SqlConnection(UserConnectionString);
                        }
                        break;
                    default:
                        {
                            throw new Exception("Database not defined");
                        }
                }
                Connection.Open();
                while (Connection.State != ConnectionState.Open)
                {
                    if (Connection.State != ConnectionState.Connecting)
                    {
                        throw new Exception("Error opening Database");
                    }
                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Close()
        {
            if (Connection != null)
            {
                Connection.Close();
            }
        }

        public SqlConnection MyConnection
        {
            get { return this.Connection; }
        }
    }
}