﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using ItendSolution.Models;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using System.Collections.Generic;
using System.Drawing.Text;
using DocumentFormat.OpenXml.Bibliography;
using Org.BouncyCastle.Asn1.Mozilla;
using System.IO;

#pragma warning disable IDE1006, IDE0017, IDE0059

namespace ItendWebAPI.Database
{
    public class UserDB
    {
        public DBSession MySession = null;

        public UserDB()
        {
            MySession = new DBSession(DBSession.MyDatabase.User);
        }


        private void _FillInfo(SqlDataReader read, UserModel user)
        {
            user.Id =read.GetInt32(read.GetOrdinal("Id"));
            user.Cid = read.GetInt32(read.GetOrdinal("Cid"));
            user.Division = read.GetString(read.GetOrdinal("Company Division"));
            user.CompanyId = read.GetString(read.GetOrdinal("CompanyId")).Trim();
            user.Alias = read.GetString(read.GetOrdinal("Alias")).Trim();
            if (read.IsDBNull(read.GetOrdinal("Password")))
            {
                user.Password = null;
            }
            else
            {
                user.Password = read.GetString(read.GetOrdinal("Password")).Trim();
            }
            user.Email = read.GetString(read.GetOrdinal("Email")).Trim();
            user.Locale = read.GetString(read.GetOrdinal("Locale"));
            user.IsAdmin = (read.GetInt32(read.GetOrdinal("IsAdmin")) != 0);
            user.IsAccounting = (read.GetInt32(read.GetOrdinal("IsAccounting")) != 0);
            user.IsUser = (read.GetInt32(read.GetOrdinal("IsUser")) != 0);
            user.ExtentedPermissions = (read.GetInt32(read.GetOrdinal("ExtendedPermissions")) != 0);
            user.Vacation = read.GetFloat(read.GetOrdinal("Vacation"));
            user.Sickdays = read.GetFloat(read.GetOrdinal("Sickdays"));
            user.WorkModel.WorkModelId = read.GetInt32(read.GetOrdinal("WorkModel"));
            user.Country = read.GetString(read.GetOrdinal("Working Country")).Trim();
            user.PerDiems = (read.GetInt32(read.GetOrdinal("Per Diems")) > 0);
        }

        public string GetUserInfo(UserModel usr)
        {
            string retval = null;
            SqlCommand command;
            SqlDataReader dataReader = null;

            try
            {
                string[] WeekColums = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
                string query = "SELECT * FROM dbo.WorkModel WHERE Id=" + usr.WorkModel.WorkModelId.ToString() +
                    " AND Remove=0";
                command = new SqlCommand(query) { Connection = MySession.Connection };
                dataReader = command.ExecuteReader();

                dataReader.Read();
                if (dataReader.HasRows)
                {
                    usr.WorkModel.WorkStarts = dataReader.GetTimeSpan(dataReader.GetOrdinal("WorkStarts"));
                    usr.WorkModel.WorkEnds = dataReader.GetTimeSpan(dataReader.GetOrdinal("WorkEnds"));
                    usr.WorkModel.LunchStart = dataReader.GetTimeSpan(dataReader.GetOrdinal("LunchStarts"));
                    usr.WorkModel.LunchEnd = dataReader.GetTimeSpan(dataReader.GetOrdinal("LunchEnds"));

                    // Get working hour for each day
                    int ind; 
                    for( ind=0; ind<7; ind++)
                    {
                        usr.WorkModel.WorkingDays[ind] = dataReader.GetFloat(dataReader.GetOrdinal(WeekColums[ind]));
                    }
                }
            }
            catch (Exception e)
            {
                retval = "DB Fail: Get User Info" + e.Message;
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }
        public UserModel Authenticate(string login, string password)
        {
            UserModel retval = null;
            SqlCommand command;
            SqlDataReader dataReader = null;

            try
            {
                string query = "SELECT * FROM dbo.UserData WHERE (Alias=\'" + login + "\' OR Email=\'" + login + "\') AND Remove=0;";
                command = new SqlCommand(query) { Connection = MySession.Connection };
                dataReader = command.ExecuteReader(CommandBehavior.SingleRow);

                dataReader.Read();
                if (dataReader.HasRows)
                {
                    retval = new UserModel();
                    _FillInfo(dataReader, retval);
                }
                // Do the hash
                if (retval.Password != null)
                {
                    PasswordHasher hassler = new PasswordHasher();
                    if (!hassler.VerifyHashedPassword(retval.Password, password))
                    {
                        retval = null;
                    }
                }
            }
            catch (Exception e)
            {
                // TODO add logger
                retval = null;
            }
            finally
            {
                if (dataReader!=null) dataReader.Close();
            }
            return retval;
        }

        public UserModel GetUserById(int id)
        {
            UserModel retval = null;
            SqlCommand command;
            SqlDataReader dataReader =null;

            try
            {
                string query = "SELECT * FROM dbo.UserData WHERE Id=" + id.ToString() + " AND Remove=0"; 
                command = new SqlCommand(query) { Connection = MySession.Connection };
                dataReader = command.ExecuteReader(CommandBehavior.SingleRow);

                dataReader.Read();
                if (dataReader.HasRows)
                {
                    retval = new UserModel();
                    _FillInfo(dataReader, retval);
                }
            }
            catch (Exception e)
            {
                // TODO add logger
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }

        public bool AddUser(UserModel user)
        {
            bool retval = true;
            SqlCommand command;
            
            // TODO check the missing columns, may be needed later.
            try 
            {
                String query = "INSERT INTO dbo.UserData (Cid,[Company Division],Verified,Inactive,Alias,Email,CompanyId, IsAdmin,IsAccounting,IsUser,ExtendedPermissions,Vacation,Sickdays,Locale,WorkModel,[Working Country],[Per Diems]) " +
                               "VALUES (@Cid,@CompanyDivision, @Verified,@Inactive,@Alias,@Email,@CompId,@IsAdmin,@IsAccounting,@IsUser,@ExtendedPermissions,@Vacation,@Sickdays,@Locale,@WorkModel,@WCountry,@PerDiems)";
                command = new SqlCommand(query) { Connection = MySession.Connection };

                SqlParameter p;
                p = command.Parameters.AddWithValue("@Cid",         user.Cid);
                if (String.IsNullOrEmpty(user.Division))
                {
                    p = command.Parameters.AddWithValue("@CompanyDivision", DBNull.Value);
                }
                else
                {
                    p = command.Parameters.AddWithValue("@CompanyDivision", user.Division.Trim());
                }
                p = command.Parameters.AddWithValue("@Verified", 0);               /// TODO User needs to verify the account via EMAIL (9.10.2020 Not done yet)
                p = command.Parameters.AddWithValue("@Inactive", 1);               /// TODO User needs to verify the account via EMAIL (9.10.2020 Not done yet)
                p = command.Parameters.AddWithValue("@Alias",       user.Alias.Trim());
                p = command.Parameters.AddWithValue("@Email",       user.Email.Trim());
                p = command.Parameters.AddWithValue("@CompId",      user.CompanyId.Trim());
                p = command.Parameters.AddWithValue("@IsAdmin",     user.IsAdmin ? 1 : 0);
                p = command.Parameters.AddWithValue("@IsAccounting",user.IsAccounting ? 1 : 0);
                p = command.Parameters.AddWithValue("@IsUser",      user.IsUser ? 1 : 0);
                p = command.Parameters.AddWithValue("@ExtendedPermissions", (user.ExtentedPermissions ? 1 : 0));
                p = command.Parameters.AddWithValue("@Vacation",    user.Vacation);
                p = command.Parameters.AddWithValue("@Sickdays",    user.Sickdays);
                p = command.Parameters.AddWithValue("@Locale",      user.Locale);
                p = command.Parameters.AddWithValue("@WorkModel",   user.WorkModel.WorkModelId);
                if (String.IsNullOrEmpty(user.Country))
                {
                    p = command.Parameters.AddWithValue("@WCountry", DBNull.Value);
                }
                else
                {
                    p = command.Parameters.AddWithValue("@WCountry", user.Country);
                }
                p = command.Parameters.AddWithValue("@PerDiems", user.PerDiems ? 1 : 0);
                command.ExecuteNonQuery();                
            }
            catch (Exception e)
            {
                // TODO logger
                retval = false;
            }
            return retval;
        }


        // TODO Add all fields into this...
        // Now only name and id is returned
        public List<UserWorkModel> GetWorkModelList(int CID)
        {
            List<UserWorkModel> retval = null;
            SqlCommand command;
            SqlDataReader dataReader=null;

            try
            {
                // TODO... all data..
                string query = "SELECT Id,Name FROM dbo.WorkModel WHERE Cid = " + CID.ToString() + ";";
                command = new SqlCommand(query) { Connection = MySession.Connection };
                dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                {
                    retval = new List<UserWorkModel>();
                    while (dataReader.Read())
                    {
                        // TODO... all data..
                        UserWorkModel model = new UserWorkModel()
                        {
                            WorkModelId = dataReader.GetInt32(dataReader.GetOrdinal("Id")),
                            WorkModelName = dataReader.GetString(dataReader.GetOrdinal("Name"))
                        };
                        retval.Add(model);
                    }
                }
            }
            catch (Exception e)
            {
                // TODO add logger
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }
    
        public UserWorkModel GetWorkModel(int Wid)
        {
            UserWorkModel retval = null;
            SqlCommand command;
            SqlDataReader dataReader = null;

            try
            {
                // TODO... all data..
                string query = "SELECT * FROM dbo.WorkModel WHERE Id = " + Wid.ToString() + ";";
                command = new SqlCommand(query) { Connection = MySession.Connection };
                dataReader = command.ExecuteReader(CommandBehavior.SingleRow);

                if (dataReader.HasRows)
                {
                    if (dataReader.Read())
                    {
                        retval = new UserWorkModel();
                        retval.WorkModelId    = dataReader.GetInt32(dataReader.GetOrdinal("Id"));
                        retval.WorkModelName  = dataReader.GetString(dataReader.GetOrdinal("Name"));
                        retval.WorkingDays[0] = dataReader.GetFloat(dataReader.GetOrdinal("Mon"));
                        retval.WorkingDays[1] = dataReader.GetFloat(dataReader.GetOrdinal("Tue"));
                        retval.WorkingDays[2] = dataReader.GetFloat(dataReader.GetOrdinal("Wed"));
                        retval.WorkingDays[3] = dataReader.GetFloat(dataReader.GetOrdinal("Thu"));
                        retval.WorkingDays[4] = dataReader.GetFloat(dataReader.GetOrdinal("Fri"));
                        retval.WorkingDays[5] = dataReader.GetFloat(dataReader.GetOrdinal("Sat"));
                        retval.WorkingDays[6] = dataReader.GetFloat(dataReader.GetOrdinal("Sun"));
                        retval.WorkStarts     = dataReader.GetTimeSpan(dataReader.GetOrdinal("WorkStarts"));
                        retval.WorkEnds       = dataReader.GetTimeSpan(dataReader.GetOrdinal("WorkEnds"));
                        retval.LunchStart     = dataReader.GetTimeSpan(dataReader.GetOrdinal("LunchStarts"));
                        retval.LunchEnd       = dataReader.GetTimeSpan(dataReader.GetOrdinal("LunchEnds"));
                    }
                }
            }
            catch (Exception e)
            {
                // TODO add logger
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return retval;
        }

        public List<UserModel> GetUserList(int Cid)
        {
            List<UserModel> RetVal = new List<UserModel>();
            UserModel User;

            SqlCommand command;
            SqlDataReader dataReader = null;
            int Column;

            try
            {
                string query = "SELECT * FROM dbo.UserData WHERE Cid = " + Cid.ToString() + " AND Remove=0";
                command = new SqlCommand(query) { Connection = MySession.Connection };
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    User = new UserModel();
                    User.Id = dataReader.GetInt32(dataReader.GetOrdinal("Id"));

                    Column = dataReader.GetOrdinal("Company Division");
                    User.Division = dataReader.IsDBNull(Column) ? "" : dataReader.GetString(Column).Trim();

                    Column = dataReader.GetOrdinal("CompanyId");
                    User.CompanyId = dataReader.IsDBNull(Column) ? "" : dataReader.GetString(Column).Trim();

                    User.IsVerified = dataReader.GetInt32(dataReader.GetOrdinal("Verified")) > 0;
                    User.IsInactive = dataReader.GetInt32(dataReader.GetOrdinal("Inactive")) > 0;

                    User.Alias = dataReader.GetString(dataReader.GetOrdinal("Alias")).Trim();
                    User.Email = dataReader.GetString(dataReader.GetOrdinal("Email")).Trim();
                    RetVal.Add(User);
                }
            }
            catch (Exception e)
            {
                // TODO add logger
            }
            finally
            {
                if (dataReader != null) dataReader.Close();
            }
            return RetVal;
        }
        public bool UpdateUser(UserModel user)
        {
            bool retval = true;

            // TODO check the missing columns, may be needed later.
            try
            {
                String query = "UPDATE dbo.UserData " +
                "SET Cid=@Cid, [Company Division]=@CompanyDivision, Verified=@Verified, Inactive=@Inactive, Alias=@Alias, Email=@Email ," +
                "CompanyId=@CompId, IsAdmin=@IsAdmin ,IsAccounting=@IsAccounting ,IsUser=@IsUser ,ExtendedPermissions=@ExtendedPermissions ," +
                "Vacation=@Vacation ,Sickdays=@Sickdays ,Locale=@Locale ,WorkModel=@WorkModel ,[Working Country]=@Country ,[Per Diems]=@PerDiems " +
                "WHERE  Id=@Id AND Remove=0;";
                using (SqlCommand command = MySession.Connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.Parameters.AddWithValue("@Id", user.Id);
                    command.Parameters.AddWithValue("@Cid", user.Cid);
                    if (String.IsNullOrEmpty(user.Division))
                    {
                        command.Parameters.AddWithValue("@CompanyDivision", DBNull.Value);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@CompanyDivision", user.Division.Trim());
                    }
                    command.Parameters.AddWithValue("@Verified", user.IsVerified?1:0);
                    command.Parameters.AddWithValue("@Inactive", user.IsInactive?1:0);
                    command.Parameters.AddWithValue("@Alias", user.Alias.Trim());
                    command.Parameters.AddWithValue("@Email", user.Email.Trim());
                    command.Parameters.AddWithValue("@CompId", user.CompanyId.Trim());
                    command.Parameters.AddWithValue("@IsAdmin", user.IsAdmin ? 1 : 0);
                    command.Parameters.AddWithValue("@IsAccounting", user.IsAccounting ? 1 : 0);
                    command.Parameters.AddWithValue("@IsUser", user.IsUser ? 1 : 0);
                    command.Parameters.AddWithValue("@ExtendedPermissions", (user.ExtentedPermissions ? 1 : 0));
                    command.Parameters.AddWithValue("@Vacation", user.Vacation);
                    command.Parameters.AddWithValue("@Sickdays", user.Sickdays);
                    command.Parameters.AddWithValue("@Locale", user.Locale);
                    command.Parameters.AddWithValue("@WorkModel", user.WorkModel.WorkModelId);
                    if (String.IsNullOrEmpty(user.Country))
                    {
                        command.Parameters.AddWithValue("@Country", DBNull.Value);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@Country", user.Country);
                    }
                    command.Parameters.AddWithValue("@PerDiems", user.PerDiems ? 1 : 0);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                // TODO logger
                retval = false;
            }
            return retval;
        }
        public bool MarkForDeletion(UserModel user)
        {
            bool retval = true;
            // TODO check the missing columns, may be needed later.
            try
            {
                String query = "UPDATE dbo.UserData SET Remove=1 WHERE  Id=@Id";
                using (SqlCommand command = MySession.Connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.Parameters.AddWithValue("@Id", user.Id);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                // TODO logger
                retval = false;
            }
            return retval;
        }

        public bool AddReport(UserRESTAPI Request)
        {
            bool retval = true;
            SqlCommand command;
            ReportModel rm;
            String query;
            int ind;
            
            try
            {
                rm = Request.ReportList[0];
                query = "DELETE FROM dbo.UserReports WHERE Uid=" + Request.RequestingUser.Id.ToString() + " AND " +
                        "[Start Year]=" + rm.Start.Year + " AND " +
                        "[Start Month]=" + rm.Start.Month;
                command = new SqlCommand(query) { Connection = MySession.Connection };
                command.ExecuteNonQuery();


                query = "INSERT INTO dbo.UserReports (Uid, Cid, [Start Year], [Start Month], [Start Day], [Start Hour], [Start Minute], " +
                        "[End Year], [End Month], [End Day], [End Hour], [End Minute], Type, Description)" +
                        "VALUES (@Uid, @Cid, @StartYear, @StartMonth, @StartDay, @StartHour, @StartMinute, " +
                        "@EndYear, @EndMonth, @EndDay, @EndHour, @EndMinute, @Type, @Description)";

                for (ind = 0; ind < Request.ReportList.Count; ind++)
                {
                    rm = Request.ReportList[ind];

                    command = new SqlCommand(query) { Connection = MySession.Connection };
                    command.Parameters.AddWithValue("@Uid", Request.RequestingUser.Id);
                    command.Parameters.AddWithValue("@Cid", Request.RequestingUser.Cid);
                    command.Parameters.AddWithValue("@StartYear", rm.Start.Year );
                    command.Parameters.AddWithValue("@StartMonth", rm.Start.Month );
                    command.Parameters.AddWithValue("@StartDay", rm.Start.Day );
                    command.Parameters.AddWithValue("@StartHour", rm.Start.Hour );
                    command.Parameters.AddWithValue("@StartMinute", rm.Start.Minute );
                    command.Parameters.AddWithValue("@EndYear", rm.End.Year );
                    command.Parameters.AddWithValue("@EndMonth", rm.End.Month );
                    command.Parameters.AddWithValue("@EndDay", rm.End.Day );
                    command.Parameters.AddWithValue("@EndHour", rm.End.Hour );
                    command.Parameters.AddWithValue("@EndMinute", rm.End.Minute );
                    command.Parameters.AddWithValue("@Type", rm.Type );
                    if (String.IsNullOrEmpty(rm.Description))
                    {
                        command.Parameters.AddWithValue("@Description", DBNull.Value);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@Description", rm.Description);
                    }
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                // TODO logger
                retval = false;
            }
            return retval;
        }

        public Stream GetReport(UserRESTAPI Request)
        {
            ReportContainer container = new ReportContainer();
            SqlCommand command;
            SqlDataReader reader = null;
            ReportModel rm;
            ReportCompiler RepComp = new ReportCompiler();
            string query;
            try
            {
                if (Request.User == null)
                {
                    query = "SELECT * FROM dbo.UserReports WHERE Cid = " + Request.RequestingUser.Cid.ToString() + " AND [Start Year]=" + Request.ReportList.PeriodStart.Year.ToString() +
                    " AND [Start Month]=" + Request.ReportList.PeriodStart.Month.ToString();
                }
                else
                {
                    query = "SELECT * FROM dbo.UserReports WHERE Cid = " + Request.RequestingUser.Cid.ToString() + " AND [Start Year]=" + Request.ReportList.PeriodStart.Year.ToString() +
                        " AND [Start Month]=" + Request.ReportList.PeriodStart.Month.ToString() + " AND Uid=" + Request.User.Id.ToString();
                }
                command = new SqlCommand(query) { Connection = MySession.Connection };
                reader = command.ExecuteReader();
                int Column;                
                while (reader.Read())
                {
                    rm = new ReportModel();
                    rm.Uid = reader.GetInt32(reader.GetOrdinal("Uid"));
                    rm.Cid = reader.GetInt32(reader.GetOrdinal("Cid"));
                    rm.Start.Year = reader.GetInt32(reader.GetOrdinal("Start Year"));
                    rm.Start.Month = reader.GetInt32(reader.GetOrdinal("Start Month"));
                    rm.Start.Day = reader.GetInt32(reader.GetOrdinal("Start Day"));
                    rm.Start.Hour = reader.GetInt32(reader.GetOrdinal("Start Hour"));
                    rm.Start.Minute = reader.GetInt32(reader.GetOrdinal("Start Minute"));

                    rm.End.Year = reader.GetInt32(reader.GetOrdinal("End Year"));
                    rm.End.Month = reader.GetInt32(reader.GetOrdinal("End Month"));
                    rm.End.Day = reader.GetInt32(reader.GetOrdinal("End Day"));
                    rm.End.Hour = reader.GetInt32(reader.GetOrdinal("End Hour"));
                    rm.End.Minute = reader.GetInt32(reader.GetOrdinal("End Minute"));
                    rm.Type = reader.GetInt32(reader.GetOrdinal("Type"));

                    Column = reader.GetOrdinal("Description");
                    rm.Description = reader.IsDBNull(Column) ? null : reader.GetString(Column);
                    container.Add(rm);
                }
            }
            catch (Exception e)
            {
                // TODO logger
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return RepComp.GenerateReport(container, Request);
        }

        public ReportContainer FetchReport(UserRESTAPI Request)
        {
            ReportContainer container = new ReportContainer();
            SqlCommand command;
            SqlDataReader reader= null;
            ReportModel rm;
            string query;

            try
            {
                query = "SELECT * FROM dbo.UserReports WHERE Cid = " + Request.RequestingUser.Cid.ToString() + " AND [Start Year]=" + Request.ReportList.PeriodStart.Year.ToString() +
                   " AND [Start Month]=" + Request.ReportList.PeriodStart.Month.ToString() + " AND Uid=" + Request.RequestingUser.Id.ToString();
                command = new SqlCommand(query) { Connection = MySession.Connection };
                reader = command.ExecuteReader();
                int Column;
                while (reader.Read())
                {
                    rm = new ReportModel();
                    rm.Uid = reader.GetInt32(reader.GetOrdinal("Uid"));
                    rm.Cid = reader.GetInt32(reader.GetOrdinal("Cid"));
                    rm.Start.Year = reader.GetInt32(reader.GetOrdinal("Start Year"));
                    rm.Start.Month = reader.GetInt32(reader.GetOrdinal("Start Month"));
                    rm.Start.Day = reader.GetInt32(reader.GetOrdinal("Start Day"));
                    rm.Start.Hour = reader.GetInt32(reader.GetOrdinal("Start Hour"));
                    rm.Start.Minute = reader.GetInt32(reader.GetOrdinal("Start Minute"));

                    rm.End.Year = reader.GetInt32(reader.GetOrdinal("End Year"));
                    rm.End.Month = reader.GetInt32(reader.GetOrdinal("End Month"));
                    rm.End.Day = reader.GetInt32(reader.GetOrdinal("End Day"));
                    rm.End.Hour = reader.GetInt32(reader.GetOrdinal("End Hour"));
                    rm.End.Minute = reader.GetInt32(reader.GetOrdinal("End Minute"));
                    rm.Type = reader.GetInt32(reader.GetOrdinal("Type"));

                    Column = reader.GetOrdinal("Description");
                    rm.Description = reader.IsDBNull(Column) ? null : reader.GetString(Column);
                    container.Add(rm);
                }
            }
            catch (Exception e)
            {
                // TODO logger
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return container;
        }

        public bool ChangePassword(UserRESTAPI Request)
        {
            PasswordHasher hassler = new PasswordHasher();
            SqlCommand command;
            string query;

            Request.Password = hassler.HashPassword(Request.Password);
            // TODO Add check for old password
            try
            {
                query = "UPDATE dbo.UserData SET Password='"+Request.Password + "' Where Id=" + Request.RequestingUser.Id.ToString();
                command = new SqlCommand(query) { Connection = MySession.Connection };
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                // TODO logger
                return false;
            }
            return true;
        }
    }
}
