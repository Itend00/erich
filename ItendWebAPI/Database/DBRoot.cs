﻿using ItendSolution.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Remoting.Messaging;

namespace ItendWebAPI.Database
{
    public static class DBRoot
    {
        private static readonly string _ConnectionString = "Data Source=DESKTOP-413NLCO;Initial Catalog=ITend;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private static SqlConnection _Connection = null;
     
        public static string Connect()
        {
            string retval = null;
            if (_Connection == null)
            {
                try
                {
                    _Connection = new SqlConnection(_ConnectionString);
                    _Connection.Open();
                }
                catch (Exception e)
                {
                    retval = e.Message;
                }
            }
            return retval;
        }

        public static void Disconnect()
        {
            if (_Connection != null)
            {
                _Connection.Close();
            }
        }

        /// <summary>
        /// Get the localized calendar related strings form the localisation database for user
        /// These are holidays, months, days,...
        /// </summary>
        /// <param name="country">Country Short Name ex. EN, FI, CZ, CA etc...</param>
        /// <param name="locale">Locale ex. Monday, January etc... </param>
        /// <param name="blong">Return long(true) or short(false) versions of the locale</param>
        public static string CalendarLocaleGetShort(string country,string locale, bool blong)
        {
            string retval;
            try
            {
                SqlCommand command;
                SqlDataReader dataReader;

                string ext = "_S"; // Defined in the database that the colum for example english is EN for long and EN_S for short
                if (blong) ext = "";

                string query = "SELECT " + country + ext + " FROM dbo.LocaleCalendar WHERE Locale=\'" + locale + "\'";
                command = new SqlCommand(query, _Connection);
                dataReader = command.ExecuteReader(CommandBehavior.SingleRow);

                dataReader.Read();
                retval = dataReader.GetString(0);

                dataReader.Close();
                command.Dispose();
            }
            catch(Exception e)
            {
                retval = "DB Fail: " + country + "->" + locale + ":" + e.Message;
            }
            return retval;
        }

        /// <summary>
        /// Get the localized week day names from the localisation database for user
        /// These are holidays, months, days,...
        /// </summary>
        /// <param name="language">Country Short Name ex. EN, FI, CZ, CA etc...</param>
        /// <param name="blong">Return long(true) or short(false) versions of the locale</param>
        public static List<string> CalendarLocaleWeekdays(string language, bool blong)
        {
            List<string> retval = new List<string>(13);
            try
            {
                SqlCommand command;
                SqlDataReader dataReader;

                string ext = "_S"; // Defined in the database that the colum is EN for long and EN_S for short
                if (blong) ext = "";

                string str = "IN (\'Monday\',\'Tuesday\',\'Wednesday\',\'Thursday\',\'Friday\',\'Saturday\',\'Sunday\')";
                string query = "SELECT " + language + ext + " FROM dbo.LocaleCalendar WHERE Locale " + str;
                command = new SqlCommand(query, _Connection);
                dataReader = command.ExecuteReader();

                int day;
                for(day=0; day<7; day++) //Just to ensure that we read only from Monday to Sunday.
                {
                    dataReader.Read();
                    retval.Add(dataReader.GetString(0));

                }
                dataReader.Close();
                command.Dispose();
            }
            catch (Exception e)
            {
                retval.Add("DB Fail: CalendarLocaleWeekdays " + language + ":" + e.Message);
                int i;
                for(i=0;i<6;i++)
                {
                    retval.Add("DB Fail");
                }
            }
            return retval;
        }

        /// <summary>
        /// Get the localized month names from the localisation database for user
        /// Returns a list of strings indexed from 1..12, 0=Dooms-ary
        /// </summary>
        /// <param name="country">Country Short Name ex. EN, FI, CZ, CA etc...</param>
        /// <param name="blong">Return long(true) or short(false) versions of the locale</param>
        public static List<string> CalendarLocaleMonths(string country, bool blong)
        {
            List<string> retval = new List<string>(13); // Numbering starts from 1=January, 12 = December
            try
            {
                SqlCommand command;
                SqlDataReader dataReader;

                string ext = "_S"; // Defined in the database that the colum is EN for long and EN_S for short
                if (blong) ext = "";

                string locale = "IN (\'January\',\'February\',\'March\',\'April\',\'May\',\'June\',\'July\',\'August\',\'September\',\'October\',\'November\',\'December\')";
                string query = "SELECT " + country + ext + " FROM dbo.LocaleCalendar WHERE Locale " + locale;
                command = new SqlCommand(query, _Connection);
                dataReader = command.ExecuteReader();

                int month;
                retval.Add("Dooms-ary");
                for (month = 0; month < 12; month++) //Just to ensure that we read only from January to December.
                {
                    dataReader.Read();
                    retval.Add(dataReader.GetString(0));
                }
                dataReader.Close();
                command.Dispose();
            }
            catch (Exception e)
            {
                retval.Add("DB Fail: CalendarLocaleWeekdays " + country + ":" + e.Message);
                int i;
                for (i = 0; i < 12; i++)
                {
                    retval.Add("DB Fail");
                }
            }
            return retval;
        }

        /// <summary>
        /// Fills the CalendarMonth object with the localized data.
        /// </summary>
        /// <param name="info"> CalendarMonth object. Must contain first day & month as input </param>
        /// <param name="language">Language to use Name ex. EN, FI, CZ, CA etc...</param>
        public static string CalendarFillInfo(CalendarMonth info, string language)
        {
            string retval = null;
            SqlCommand command = null;
            SqlDataReader dataReader = null;

            try
            {
                string llong = language;
                string lshort = language + "_S";

                List<string> locales = new List<string> {"January","February","March","April","May","June","July","August","September","October","November","December" };

                string query = "SELECT " + " " + llong + "," + lshort + " FROM dbo.LocaleCalendar WHERE Locale =" + "\'" + locales[info.Month - 1] + "\'";
                command = new SqlCommand(query, _Connection);
                dataReader = command.ExecuteReader();
                dataReader.Read();
                info.MonthLong = dataReader.GetString(dataReader.GetOrdinal(llong));
                info.MonthShort =  dataReader.GetString(dataReader.GetOrdinal(lshort));
                dataReader.Close(); dataReader = null;
                command.Dispose(); command = null;

                // Here we assume that the DB returns weekdays in order mon,tue,wed....
                string unionstr = "(\'Monday\',\'Tuesday\',\'Wednesday\',\'Thursday\',\'Friday\',\'Saturday\',\'Sunday\')";
                query = "SELECT " + " " + llong + "," + lshort + " FROM dbo.LocaleCalendar WHERE Locale IN " + unionstr;
                command = new SqlCommand(query, _Connection);
                dataReader = command.ExecuteReader();

                for (int i=0; i<7; i++)
                {
                    dataReader.Read();
                    info.DayNamesLong[i] = dataReader.GetString(dataReader.GetOrdinal(llong));
                    info.DayNamesShort[i] = dataReader.GetString(dataReader.GetOrdinal(lshort));
                }

                dataReader.Close(); dataReader = null;
                command.Dispose(); command = null;
            }
            catch (Exception e)
            {
                if (dataReader != null) dataReader.Close();
                if (command != null) command.Dispose();
                retval = "DB Fail: CalendarFillInfo " + language + ":" + e.Message;
            }

            return retval;
        }
    }
}
