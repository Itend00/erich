﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ItendWebAPI.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string alpha2 { get; set; }
        public string alpha3 { get; set; }
        public string number { get; set; }

        /// <summary>
        /// Justin Case
        /// </summary>
        public string Name
        {
            get { return name.Trim(); }
        }
        /// <summary>
        /// Justin Case
        /// </summary>
        public string Alpha2
        {
            get { if (Id == 0) return null; return this.alpha2.Substring(0, 2); }
        }
        /// <summary>
        /// Justin Case
        /// </summary>
        public string Alpha3
        {
            get { if (Id == 0) return null; else return this.alpha3.Substring(0, 3); }
        }
        /// <summary>
        /// Justin Case
        /// </summary>
        public int Number
        {
            get { if (Id == 0) return 0; else return int.Parse(this.number); }
        }
        /// <summary>
        /// Justin Case
        /// </summary>
        public string NumberStr
        {
            get 
            {
                if (Id == 0)
                {
                    return null;
                }
                else
                {
                    string tmp = "  " + this.number;
                    return tmp.Substring(tmp.Length - 3, 3);
                }
            }
        }

        public Country()
        {
            this.Id = 0;
        }
    }
    public class CountryIBANList
    {
        public List<Country> Items { get; set; }

        public CountryIBANList()
        {
            this.Items = new List<Country>();
        }
    }
}