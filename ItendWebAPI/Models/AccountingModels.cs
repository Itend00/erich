﻿using System.Collections.Generic;

namespace ItendSolution.Models
{
    public class ReportRequest
    {
        /// <summary>
        /// ID of who is requesting ?
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Requester company ID
        /// </summary>
        public int Cid { get; set; }
        /// <summary>
        /// Which year the report will be made
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Which month the report will be made
        /// </summary>
        public int Month { get; set; }
        /// <summary>
        /// If != 0 then specific user report requested.
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// If true show vacations, sickdays, etc...
        /// </summary>
        public bool Absenses { get; set; }
        /// <summary>
        /// If true include travel report
        /// </summary>
        public bool Travels { get; set; }
        /// <summary>
        /// Depending on the country the day may need to be divided into different periods.
        /// Depends also on the labor law and agreenments between unions.
        /// Example: evening work, night work, etc...
        /// </summary>
        public DaySplit SplitHours { get; set; }
    }

    public class DayPeriod
    {
        public string Name { get; set; }
        public MyDateTime Start { get; set; }
        public MyDateTime End { get; set; }
        public DayPeriod()
        {
            this.Name = "----";
            this.Start = new MyDateTime();
            this.End = new MyDateTime();
        }
    }
    public class DaySpecial
    {
        public string Name { get; set; }
        public MyDateTime Date { get; set; }
        public DaySpecial()
        {
            this.Name = null;
            this.Date = new MyDateTime();
        }
    }
    public class DaySplit
    {
        public string Name { get; set; }
        /// <summary>
        /// Normal week
        /// </summary>
        public List<List<DayPeriod>> Week { get; set; }
        /// <summary>
        /// Special days like x-mas that may need different handling.
        /// </summary>
        public List<DaySpecial> Special { get; set; }

        public int CountWeek
        {
            get
            {
                return this.Week.Count;
            }
        }

        public int CountSpecial
        {
            get
            {
                return this.Special.Count;
            }
        }

        public DaySplit()
        {
            this.Week = new List<List<DayPeriod>>();
            this.Special = new List<DaySpecial>();
            for( int i=1; i<8; i++) // Monday = 1, sunday = 7
            {
                List<DayPeriod> ldp = new List<DayPeriod>();
                this.Week.Add(ldp);
            }
        }
    }

    public class AccountingModels
    {
    }
}