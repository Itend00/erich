﻿
using System;
using System.IO;
using System.Collections.Generic;
using ItendSolution.Shared;
using System.Security.Cryptography;
using System.Runtime.CompilerServices;

namespace ItendSolution.Models
{
    /// <summary>
    /// Working time configuration for a user.
    /// </summary>
    public class UserWorkModel
    {
        public int WorkModelId { get; set; }
        public string WorkModelName { get; set; }
        /// <summary>
        /// 0..7 0=Monday, if the user works on those days and how many hours.
        /// </summary>
        public float[] WorkingDays { get; set; }
        public TimeSpan WorkStarts { get; set; }
        public TimeSpan WorkEnds { get; set; }
        public TimeSpan LunchStart { get; set; }
        public TimeSpan LunchEnd { get; set; }

        /// <summary>
        /// Parse number of working hours from string "7.5 8 10 12" Max nof numbers is 7 (mon-sun)
        /// </summary>
        /// <param name="hours">String of working hours "5.5 6.6 7.8"...</param>
        public UserWorkModel()
        {
            WorkModelId = 0;
            WorkModelName = "";
            WorkingDays = new float[7];
            WorkStarts = new TimeSpan();
            WorkEnds = new TimeSpan();
            LunchStart = new TimeSpan();
            LunchEnd = new TimeSpan();
        }
    }

    public class UserModel
    {
        /// <summary>
        /// User authenticated or not 
        /// </summary>
        public bool Authenticated { get; set; }

        /// <summary>
        /// If the user has verified his details via email or something.
        /// </summary>
        public bool IsVerified { get; set; }

        /// <summary>
        /// If the user has been marked as inactive
        /// </summary>
        public bool IsInactive { get; set; }

        /// <summary>
        /// User self selected alias/username. We do not want to use iny info that identifies user (GDPR)
        /// </summary>
        public string Alias { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        /// <summary>
        /// Identify user in DB        
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Identifies the company
        /// </summary>
        public int Cid { get; set; }
        /// <summary>
        /// User id in the company system
        /// </summary>
        public string CompanyId { get; set; }
        /// <summary>
        /// Company Division
        /// </summary>
        public string Division { get; set; }
        /// <summary>
        /// Locale defines the languate to be used: EN, FI, CZ, ....
        /// </summary>
        /// 
        public string Locale { get; set; }

        /// <summary>
        /// Country is the country we use to display the calendar. 
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// If travel expenses are calculated using Per Diems instead of Daily Allowance
        /// </summary>
        public bool PerDiems { get; set; }

        /// <summary>
        /// Basic user access levels: Admin can do almost anything. Create/change/delete new users, create/change/delete working models, assing user levels.
        /// Not able to change company info or control application instanse related data.
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Can fetch reports, lock months, (TODO: Fix user reports)
        /// </summary>
        public bool IsAccounting { get; set; }

        /// <summary>
        /// Can report time and see own reports.
        /// </summary>
        public bool IsUser { get; set; }

        /// <summary>
        /// TODO: User can be allowed to have higher permissions temporalily. Admin must change this.
        /// Also may need revising if there are some specific rights to be elevated.
        /// </summary>
        public bool ExtentedPermissions { get; set; }

        /// <summary>
        /// How many days of vation left.
        /// </summary>
        public float VacationLeft { get; set; }

        /// <summary>
        /// Total number of vacation days. When this is changed depends on the country and must be inserted into system separately.
        /// </summary>
        public float Vacation { get; set; }

        /// <summary>
        /// Sickdays left. Need to be float as some may allow to take half day as a sick day.
        /// </summary>
        public float SickdaysLeft { get; set; }

        /// <summary>
        /// Total number of sickdays. In practice int, but just for counting we use float.
        /// </summary>
        public float Sickdays { get; set; }

        public UserWorkModel WorkModel { get; set; }

        public UserModel()
        {
            this.Id = 0;
            this.CompanyId = "";
            this.Division = "";
            this.Alias = "";
            this.Email = "";
            this.Authenticated = false;
            this.IsAccounting = false;
            this.IsAdmin = false;
            this.IsUser = false;
            this.Locale = "";
            this.Country = "";
            WorkModel = new UserWorkModel();
        }
    }

    /// <summary>
    /// In this class we define all the necessary user data to be sent and received via net.
    /// This is done in order to reuse the same POST method for HTTP for generic purposes
    /// If you need some more data just add it here.
    /// </summary>
    public class UserRESTAPI
    {
        /// <summary>
        /// Sender: CLIENT
        /// Check the SharedDefinitions.cs
        /// </summary>
        public int Action { get; set; }

        /// <summary>
        /// Sender: CLIENT
        /// Used to get one user, workmodel or such from the server.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Sender: SERVER 
        /// Response, If the request was successful 
        /// </summary>         
        public bool Success { get; set; }

        /// <summary>
        /// If you want to send a message one way or the other. Mainly used for debugging purposes.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Sender: CLIENT
        /// For logging user in, not to be used in other places.
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Sender: CLIENT
        /// For logging user in, not to be used in other places.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Sender: CLIENT 
        /// for server to check if the requester has rights to do the request
        /// Server sends this back to client as is.
        /// </summary>
        public UserModel RequestingUser { get; set; }

        /// <summary>
        /// Sender: SERVER
        /// Response from server. 
        /// </summary>
        public List<UserModel> UserList { get; set; }

        /// <summary>
        /// Sender: SERVER, Client
        /// Server -> In case only one user is requested
        /// Client -> Add user
        /// Clinet -> Fecth Report for specific user
        /// </summary>
        public UserModel User { get; set; }

        /// <summary>
        /// Sender: SERVER
        /// Workmodels, needed for some actions
        /// </summary>
        public List<UserWorkModel> WorkModelList { get; set; }

        public ReportContainer ReportList { get; set; }

        public byte[] BinaryData { get; set; }


        /// <summary>
        /// Sender: SERVER, CLIENT
        /// Server -> Workmodel get.
        /// CLinet -> Workmodel Add.
        /// </summary>
        public UserWorkModel WorkModel { get; set; }

        public UserRESTAPI()
        {
            Action = RestAPIUser.Undefined;
            Success = false;
            Id = 0;
            Login = "";
            Password = "";
            Message = "";
            RequestingUser = null;
            UserList = null;
            WorkModelList = null;
            User = null;
            ReportList = null;
            WorkModel = null;
        }
    }

    public sealed class PasswordHasher
    {
        public byte Version => 1;
        public int Pbkdf2IterCount { get; } = 50000;
        public int Pbkdf2SubkeyLength { get; } = 256 / 8; // 256 bits
        public int SaltSize { get; } = 128 / 8; // 128 bits
        public HashAlgorithmName HashAlgorithmName { get; } = HashAlgorithmName.SHA256;

        public string HashPassword(string password)
        {
            if (password == null)
                throw new ArgumentNullException(nameof(password));

            byte[] salt;
            byte[] bytes;
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, SaltSize, Pbkdf2IterCount, HashAlgorithmName))
            {
                salt = rfc2898DeriveBytes.Salt;
                bytes = rfc2898DeriveBytes.GetBytes(Pbkdf2SubkeyLength);
            }

            var inArray = new byte[1 + SaltSize + Pbkdf2SubkeyLength];
            inArray[0] = Version;
            Buffer.BlockCopy(salt, 0, inArray, 1, SaltSize);
            Buffer.BlockCopy(bytes, 0, inArray, 1 + SaltSize, Pbkdf2SubkeyLength);

            return Convert.ToBase64String(inArray);
        }


        /// <summary>
        /// Check the pasword
        /// </summary>
        /// <param name="hashedPassword"></param>
        /// <param name="password"></param>
        /// <returns>true if match, false if not</returns>
        public bool VerifyHashedPassword(string hashedPassword, string password)
        {
            if (password == null) return false;

            if (hashedPassword == null) return false;

            byte[] numArray = Convert.FromBase64String(hashedPassword);
            if (numArray.Length < 1) return false;

            byte version = numArray[0];
            if (version > Version) return false;

            byte[] salt = new byte[SaltSize];
            Buffer.BlockCopy(numArray, 1, salt, 0, SaltSize);
            byte[] a = new byte[Pbkdf2SubkeyLength];
            Buffer.BlockCopy(numArray, 1 + SaltSize, a, 0, Pbkdf2SubkeyLength);
            byte[] bytes;
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt, Pbkdf2IterCount, HashAlgorithmName))
            {
                bytes = rfc2898DeriveBytes.GetBytes(Pbkdf2SubkeyLength);
            }

            if (FixedTimeEquals(a, bytes)) return true;

            return false;
        }

        // In .NET Core 2.1, you can use CryptographicOperations.FixedTimeEquals
        // https://github.com/dotnet/runtime/blob/419e949d258ecee4c40a460fb09c66d974229623/src/libraries/System.Security.Cryptography.Primitives/src/System/Security/Cryptography/CryptographicOperations.cs#L32
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public static bool FixedTimeEquals(byte[] left, byte[] right)
        {
            // NoOptimization because we want this method to be exactly as non-short-circuiting as written.
            // NoInlining because the NoOptimization would get lost if the method got inlined.
            if (left.Length != right.Length)
            {
                return false;
            }

            int length = left.Length;
            int accum = 0;

            for (int i = 0; i < length; i++)
            {
                accum |= left[i] - right[i];
            }

            return accum == 0;
        }
    }
}
