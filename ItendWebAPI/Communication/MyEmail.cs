﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace ItendWebAPI.Communication
{
    public class MyEmail
    {
        private MailAddress addSender;   // You can mess the sender info in case you need or not a response.
        private MailAddress addReceiver;
        private string strTopic;

        public MyEmail()
        {
            // Not a thingy to do here now.
        }

        public MyEmail(string sender, string receiver, string topic)
        {
            this.addSender = new MailAddress(sender);
            this.addReceiver = new MailAddress(receiver);
            this.strTopic = topic;
        }

        public string Sender
        {
            get { return this.Sender.ToString(); }
            set { this.addSender = new MailAddress(value); }
        }

        public string Receiver
        {
            get { return this.addReceiver.ToString(); }
            set { this.addReceiver = new MailAddress(value); }
        } 

        public string Topic
        {
            get { return this.strTopic; }
            set { strTopic = value; }
        }

        public void SendMessage(string msg)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = this.addSender;
                message.To.Add(this.addReceiver.ToString());
                message.Subject = this.Topic;
                message.IsBodyHtml = false;
                message.Body = msg;
                smtp.Port = 587;
                smtp.Host = "smtp.mail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("itend@mail.com", "madamima");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception) { }
        }
    }    
}