﻿using Org.BouncyCastle.Asn1.Cms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

/*
 * Template class. Can be used with any text ex. HTML to replace certain texts etc... to use for examle when sending HTML
 * format email. All the strings that are to be replaced are marked in the template with \$<KAKA>\$.
 * the string "Hello \$KAKA\$" will be replaced as "Hello Madam!"
 * 
 *  MyTemplate template = new MyTemplate(); // or MyTemplate("Hello \$KAKA\$");
 *  template.template = "Hello \$KAKA\$";
 *  template.Add("KAKA","Madam!");
 *  template.Translate();
 *  string final = template.Final;
 *
 *  or as a memory stream if needed to send a file over the NET.
 *  
 */

namespace ItendWebAPI.Communication
{
    public class MyTemplate
    {
        private List<string> _tokens;
        private List<string> _texts;
        private string       _template;
        private string       _final;

        private void _Init()
        {
            this._tokens = new List<string>();
            this._texts = new List<string>();
            this._template = "";
            this._final = "";
        }

        public MyTemplate()
        {
            _Init();
        }
        

        public MyTemplate(string strTemplate)
        {
            _Init();
            this._template = strTemplate;
        }

        public int Add(string strToken, string strText)
        {
            this._tokens.Add(strToken);
            this._tokens.Add(strText);
            return this._tokens.Count();
        }

        public void Translate()
        {
            string toc = "";
            int startInd = 0;
            int endInd = this._template.Length;
            int tocInd;
            this._final = "";

            startInd = this._template.IndexOf("\\$", startInd);
            if (startInd > -1)
            {
                while (startInd > -1)
                {
                    this._final += this._template.Substring(endInd, endInd - startInd);
                    startInd += 2;
                    endInd = this._template.IndexOf("\\$", startInd);
                    if (endInd > -1)
                    {
                        toc = this._final += this._template.Substring(endInd, endInd - startInd);
                        tocInd = this._tokens.IndexOf(toc);
                        if (tocInd > -1)
                        {
                            this._final += this._texts[tocInd];
                        }
                        else
                        {
                            Exception ex = new Exception("Template error, token not found: " + toc);
                        }
                        endInd += 2;
                        startInd = this._template.IndexOf("\\$", endInd);
                    }
                    else
                    {
                        Exception ex = new Exception("Template error, missing token marker");
                        throw ex;
                    }
                }
                this._final += this._template.Substring(endInd, this._template.Length - endInd);
            }
            else
            {
                this._final = this._template;
            }
        }
        public string Final
        {
            get { return this._final; }
        }

        public Stream streamFinal
        {
            get
            {
                Stream s = new MemoryStream();
                StreamWriter sw = new StreamWriter(s);
                sw.Write(this._final);
                sw.Flush();
                s.Position = 0;
                return s;
            }
        }
    }
}