﻿
using System;

namespace ItendWebAPI.Areas
{
    /// <summary>
    /// Conversion methods for time and date.
    /// Needed conversion between database/FE components/Models.
    /// </summary>
    public class MyConverter
    {
        /// <summary>
        /// Separator to be used between hour and minute when converting DateTime to string.
        /// Does not have impact on string to DateTime. 
        /// </summary>
        private readonly char Separator = '-'; 
        /// <summary>
        /// Converts DateTime format to string format "10:23" or specifically "HH[separator]MM"
        /// This is needed for converting between 
        /// </summary>
        /// <param name="time">DateTime to convert</param>
        /// <param name="separator">Separator to be used between hour and minute</param>
        /// <returns>Time in string format HH[separator]MM</returns>
        public string ConvertTimeToString(DateTime time)
        {
            string str, tmp;
            tmp = "00" + time.Hour.ToString();
            str = tmp.Substring(0, 2) + this.Separator;
            tmp = "00" + time.Minute.ToString();
            str += tmp.Substring(0, 2);
            return str;
        }
        /// <summary>
        /// Convert string format time ex "03:30" or "10-20" to DateTime format.
        /// The input must be of format "HH[any char]MM"
        /// </summary>
        /// <param name="str">Time string to convert</param>
        /// <returns>DateTime which contains only hour and minute other fields are 0.</returns>
        public DateTime ConvertStringToTime(string str)
        {
            return new DateTime(0, 0, 0, int.Parse(str.Substring(0, 2)), int.Parse(str.Substring(3, 2)), 0);
        }

        /// <summary>
        /// Constructor and sets the separator charater between hour and minute.
        /// </summary>
        /// <param name="separator">separator between hour and minute "hh[separator]mm"</param>
        public MyConverter(char separator)
        {
            this.Separator = separator;
        }

        public MyConverter()
        {
            this.Separator = ' ';
        }
    }
}