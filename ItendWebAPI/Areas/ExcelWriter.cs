﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using Org.BouncyCastle.Operators;
using SpreadsheetLight;

namespace ItendWebAPI.Areas
{
    public class ExcelWriter
    {

        public SLDocument Doc;
        public int NextRow;
        public string error;

        public ExcelWriter()
        {
            Doc = new SLDocument(SLThemeTypeValues.Foundry);
            NextRow = 1;
        }

        /// <summary>
        /// {'C:\\Program Files (x86)\\IIS Express\\ExcelReportTemplate.xlsx'."}
        /// </summary>
        /// <param name="TemplateFile"></param>
        public ExcelWriter(string TemplateFile)
        {
            try
            {
                Doc = new SLDocument(TemplateFile);
            }
            catch(Exception e)
            {
                Doc = null;
                error = e.Message;
            }
            finally
            {
                if (Doc == null)  Doc = new SLDocument(SLThemeTypeValues.Foundry);
            }
            NextRow = 1;
        }

        public Stream WriteToStream()
        {
            MemoryStream stream = new MemoryStream();
            Doc.SaveAs(stream);
            return stream;
        }

        public void AddColumnHeaders(int Row, int StartCol,List<string> HeaderList)
        {
            List<object> oList = new List<object>();
            foreach(string s in HeaderList)
            {
                oList.Add(s);
            }
            this.NextRow = Row;
            this.AddValues(StartCol, oList);
        }

        public void SetCellValue(int row, int col, Object value)
        {
            switch (Type.GetTypeCode(value.GetType()))
            {
                case TypeCode.String:
                    {
                        Doc.SetCellValue(row, col, (string)value);
                    }
                    break;
                case TypeCode.Int32:
                    {
                        Doc.SetCellValue(row, col, (int)value);
                    }
                    break;
                case TypeCode.Single:
                    {
                        Doc.SetCellValue(row, col, (float)value);
                    }
                    break;
            }
        }

        public void AddValues(int StartCol, Object ListValues)
        {
            int i;

            Doc.SetCellValue(10, 10, error);
            List<Object> ListVal = (List<Object>)ListValues;

            for (i = 0; i < ListVal.Count; i++)
            {
                this.SetCellValue(NextRow, StartCol + i, (string)ListVal[i]);
            }
            NextRow++;
        }
        
        public void WriteNamedCells(List<string> ListCells, List<string> Values)
        {
            int i;
            string DefName;
            string cell;
            int p1, p2;
            for ( i=0; i<ListCells.Count; i++)
            {
                DefName = Doc.GetDefinedNameText(ListCells[i]);
                
                // No need for temlates to have all the possible defined fields
                if (DefName.Length > 3) // 0 would be enough....
                {
                    p1 = DefName.IndexOf('$');
                    p2 = DefName.Substring(p1 + 1).IndexOf('$');
                    cell = DefName.Substring(p1 + 1, p2);
                    p1 += p2 + 2;
                    cell += DefName.Substring(p1, DefName.Length-p1);
                    Doc.SetCellValue(cell, Values[i]);
                }
            }
        }

        public string GetCellValueString(int row, int col)
        {
            return Doc.GetCellValueAsString(row, col);
        }

        public int GetCellValueInt(int row, int col)
        {
            return Doc.GetCellValueAsInt32(row, col);
        }

        public int GetNamedRow(string Cell)
        {
            int p1, p2;
            p1 = Cell.IndexOf('$');
            p2 = Cell.Substring(p1 + 1).IndexOf('$');
            p1 += p2 + 2;
            return int.Parse(Cell.Substring(p1, Cell.Length - p1));
        }

        /// <summary>
        /// Only works with one letter [A..Z] not with AA,BA etc..
        /// </summary>
        /// <param name="Defined_Name"></param>
        /// <returns></returns>
        public int GetNamedCol(string Cell)
        {
            int p1, p2;
            char Col;
            p1 = Cell.IndexOf('$');
            p2 = Cell.Substring(p1 + 1).IndexOf('$');
            Col = (char)Cell.Substring(p1 + 1, p2).ToCharArray(0,1)[0];
            p1 = Col - 'A' + 1;
            return p1;
        }

        public string GetNamedAddress(string name)
        {
            return Doc.GetDefinedNameText(name);
        }

        public void SelectSheet(string name)
        {
            Doc.SelectWorksheet(name);
        }
    }
}