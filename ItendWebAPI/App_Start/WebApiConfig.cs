﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Cors;
using System.Web.Http.Cors;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;

namespace ItendWebAPI
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method,
            AllowMultiple = false)]
    public class GlobalEnableCorsAttribute :Attribute, ICorsPolicyProvider
    {
        public Boolean SupportsCredentials = true;
        public CorsHandler handle = new CorsHandler();
        
        public async Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var corsRequestContext = request.GetCorsRequestContext();
            var originRequested = corsRequestContext.Origin;

            string approvedOrigin = handle.approveCorsOrigin(originRequested);

            if (!string.IsNullOrEmpty(approvedOrigin))
            {
                // Grant CORS request
                var policy = new CorsPolicy
                {
                    AllowAnyHeader = true,
                    AllowAnyMethod = true,
                    AllowAnyOrigin = true,
                    SupportsCredentials = true
                };

                // add headers
               
                //policy.Headers.Add("content-type");
                //policy.Headers.Add("withcredentials");
                //policy.Headers.Add("Access-Control-Allow-Headers");
                //policy.Headers.Add("Access-Control-Allow-Origin: *");
                //policy.Headers.Add("Origin");
                //policy.Headers.Add("Accept");
                //policy.Headers.Add("X-Requested-With");
                //policy.Headers.Add("Access-Control-Request-Method");
                //policy.Headers.Add("Access-Control-Request-Headers");
               
                //policy.Headers.Add("Access-Control-Allow-Origin: *");
                //policy.Headers.Add("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS, CREATE");
                //policy.Headers.Add("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token");


                policy.Origins.Add(approvedOrigin);
                return policy;
            }
            else
            {
                // Reject CORS request
                return null;
            }
        }
    }
    public class CorsHandler
    {
        public string approveCorsOrigin(string providedOrigin)
        {
            // load list of web.config origins

            // TODO: When using local version uncomment fullList = https://localhost:44354
            // string fullList = "https://localhost:44354";
            //string fullList = "https://localhost:5001";
            // string fullList = Properties.Settings.Default.CORSOriginPermittedSite;

            //if (!string.IsNullOrEmpty(fullList))
            //{
            //    string[] originArray = fullList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            //    foreach (string approvedOrigin in originArray)
            //    {
            //        if (providedOrigin.Trim() == approvedOrigin.Trim())
            //        {
            //            return providedOrigin;
            //        }
            //    }
            //}

            // TODO This is to make sure that we can access this anywhere..... Need to remove and make proper for production version.
            return providedOrigin;            
        }
    }
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // TODO Cors just for development
            //var cors = new GlobalEnableCorsAttribute();
            // config.EnableCors();
            
            
            // Web API routes
            
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
