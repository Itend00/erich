BUGS:
	User List:
		 updated after making changes to the user data.
	Calendar:
		Prefill now works -> Root cause: the workmodel was not loaded during authentication. Happened after updating the Authenticate system.

Changes:
	Admin pages removed
		Functionality added directly to the NavMenu on the left.