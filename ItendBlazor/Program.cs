using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Blazored.Modal;
using ItendBlazor.Services;

namespace ItendBlazor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<ItendBlazor.App>("app");

            builder.Services.AddTransient(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddBlazoredModal();
            builder.Services.AddSingleton<UserService>();
            builder.Services.AddSingleton<LocaleService>();
            builder.Services.AddSingleton<RefreshService>();
            builder.Services.AddSingleton<CalendarService>();
            builder.Services.AddSingleton<AccountingService>();

            var host = builder.Build();
            var userService = host.Services.GetRequiredService<UserService>();
            var localeService = host.Services.GetRequiredService<LocaleService>();
            var refreshService = host.Services.GetRequiredService<RefreshService>();
            var calendarService = host.Services.GetRequiredService<CalendarService>();
            var accountingService = host.Services.GetRequiredService<AccountingService>();
            
            var client = new HttpClient()
            {
                BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)
            };

            builder.Services.AddTransient(sp => client);
            var response = await client.GetAsync("config.json");
            var stream = await response.Content.ReadAsStreamAsync();
            builder.Configuration.AddJsonStream(stream);

            await host.RunAsync();
        }
    }
}