﻿using ItendSolution.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace ItendBlazor.Services
{
    /// <summary>
    /// Clien side component that reduces the trafic between server and client.
    /// Makes sure that only those translations that are needed are fecthed from the server.
    /// 
    /// NOT SUPPORTED IN WebAssembly...
    /// 
    /// </summary>
    public class LocaleService
    {
        /// <summary>
        /// Holds all the transalations and asks.
        /// </summary>
        public LocaleModel Loc;
        /// <summary>
        /// Holds boolean values if Locale in Loc (Localemodel) is already translated.
        /// </summary>
        public List<bool> Exists;

        /// <summary>
        /// In case the locale is changed during navigation needs to inform pages that the state has changed.
        /// </summary>
        public event Action OnChange;
        private void NotifyStateChanged() => OnChange?.Invoke();


        public LocaleService()
        {
            Loc = new LocaleModel();
            Exists = new List<bool>();
        }

        public async Task SetLocale(string L)
        {
            int i;
            this.Loc.Lang = L;
            for (i = 0; i < Exists.Count; i++) Exists[i] = false;            
            await this.UpdateFromServerAsync();
            this.NotifyStateChanged();
        }
        public void Add(string locale)
        {
            int ind;
            // Check if this is already in the list
            ind = this.Loc.Locale.IndexOf(locale);
            if (ind == -1) // Needs to be added
            {
                this.Loc.AddLocale(locale);
                Exists.Add(false);
            }
        }

        public void AddList(List<string> list)
        {
            foreach(string str in list)
            {
                this.Add(str);
            }
        }

        /// <summary>
        /// Get the locale directy from memory.
        /// Assuming that the local has already been fetched from the server using UpdateFromServer
        /// </summary>
        /// <param name="locale"></param>
        /// <returns></returns>
        public string Get(string locale)
        {
            int ind;
            string retval = "******";
            ind = this.Loc.Locale.IndexOf(locale);
            if (ind > -1) // Needs to be added
            {
                if (!Exists[ind])
                {
                    retval = "Not fetched";
                }
                else
                {
                    retval = this.Loc.Translation[ind];
                }
            }
            return retval;
        }

              /// <summary>
        /// Updates the non translated locales from the server.
        /// Those which have already been translated are not fetched again from the server.
        /// </summary>
        /// <returns></returns>
        public async Task UpdateFromServerAsync()
        {
            int i;
            LocaleModel L = new LocaleModel(this.Loc.Lang);

            // Fecth only the ones that are not already translated.
            // Let's do this in a simple loop.
            for (i = 0; i < Exists.Count; i++)
            {
                if (!Exists[i]) // If not translated
                {
                    L.AddLocale(Loc.Locale[i]);
                    Exists[i] = true; // At least we try... But to avoid fecthing the same for multiple times.
                }
            }

            var json = JsonConvert.SerializeObject(L);
            var data = new StringContent(json, Encoding.UTF8, "text/json");

            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/locale"), data);
            string result = response.Content.ReadAsStringAsync().Result;
            L = JsonConvert.DeserializeObject<LocaleModel>(result);            
            this.Loc.Merge(L);
        }

        /// <summary>
        /// Special to fetch the list of supported Countries and Languages        
        /// </summary>
        /// <returns>SupportedCountriesModel</returns>
        public async Task<SupportedCountriesModel> GetSupportedCountries()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(Conf.MakeServerAddress("api/Country/List"));
            string result = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<SupportedCountriesModel>(result);
        }
    }
}
