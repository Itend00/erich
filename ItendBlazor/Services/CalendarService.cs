﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;
using ItendSolution.Models;
using Newtonsoft.Json;
using System.Net.Http;

namespace ItendBlazor.Services
{
    public class CalendarService
    {
        public CalendarMonth Month { get; set; }

        public CalendarService()
        {
            Month = new CalendarMonth();
        }

        public async Task<CalendarMonth> FetchCalendar(int year, int month, string locale)
        {
            var client = new HttpClient();
            var response = await client.GetAsync(Conf.MakeServerAddress("api/calendar?year=") + year.ToString() + "&month=" + month.ToString() + "&lang=" + locale);
            string result = response.Content.ReadAsStringAsync().Result;
            Month = JsonConvert.DeserializeObject<CalendarMonth>(result);
            return Month;
        }

        public string[] GetShortDayNames()
        {
            return Month.DayNamesShort;
        }
        public string[] GetLongDayNames()
        {
            return Month.DayNamesLong;
        }
    }
}
