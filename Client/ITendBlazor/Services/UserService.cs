﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ItendSolution.Models;
using ItendSolution.Shared;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;

namespace ItendBlazor.Services
{
    public class UserService
    {
        public UserModel User { get; set; }

        public bool UserAuthenticated
        {
            get { return User.Authenticated; }
        }

        public string Alias
        {
            get { return User.Alias; }
        }

        public bool Admin
        {
            get { return User.IsAdmin; }
        }
        public bool Accounting
        {
            get { return User.IsAccounting; }
        }

        public string Locale
        {
            get { return User.Locale; }
        }
        public UserService()
        {
            User = new UserModel();
        }

        public async Task<bool>AuthenticateUser(string login, string password)
        {

            UserRESTAPI rest = new UserRESTAPI()
            {
                Action = RestAPIUser.AuthenticateUser,
                Login = login,
                Password = password,
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;
            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            if (rest.Success)
            {
                this.User = rest.User;
            }
            return rest.Success;
        }

        public async Task<bool> ExistingUser(string login, string password)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Action = RestAPIUser.ExistingUser,
                Login = login,
                Password = password,
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;
            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            if (rest.Success)
            {
                this.User = rest.User;
            }
            return rest.Success;
        }

        public async Task<List<UserWorkModel>> GetWorkModelList()
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Action = RestAPIUser.WorkmodelList,
                RequestingUser = this.User                
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;

            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            return rest.WorkModelList;
        }

        public async Task<UserWorkModel> GetWorkModel(int Wid)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Id = Wid,
                Action = RestAPIUser.Workmodel,
                RequestingUser = this.User                
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;

            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            return rest.WorkModel;
        }


        // TODO GOOD TO HAVE make a function that shortens this shit below, the same functionality several times.
        // TODO MUST HAVE check user rights to perform a task.
        public async Task<bool> AddUser(UserModel user)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Action = RestAPIUser.AddUser,
                RequestingUser = this.User,
                User = user
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;

            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);            
            return rest.Success;
        }

        public async Task<List<UserModel>> GetUserList()
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Action = RestAPIUser.UserList,
                RequestingUser = this.User
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json"); 
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;

            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            return rest.UserList;
        }

        public async Task<UserModel> GetUser(int Uid)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Id = Uid,
                Action = RestAPIUser.GetUser,
                RequestingUser = this.User
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;

            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            return rest.User;
        }

        public async Task UpdateUser(UserModel usr)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                User = usr,
                Action = RestAPIUser.UpdateUser,
                RequestingUser = this.User
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
        }

        public async Task MarkForDeletion(UserModel usr)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                User = usr,
                Action = RestAPIUser.MarkForDeletion,
                RequestingUser = this.User
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            await client.PostAsync(Conf.MakeServerAddress("api/User"), data);            
        }

        /// <summary>
        /// Get user reports from server for selected user and period
        /// </summary>
        /// <param name="Uid">User ID</param>
        /// <param name="PStart">MyDateTime Year, Month and Day are used</param>
        /// <param name="PEnd"></param>
        /// <returns></returns>
        public async Task<ReportContainer> FetchReport(MyDateTime PStart, MyDateTime PEnd)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Action = RestAPIUser.FetchReport,
                RequestingUser = this.User,                
            };

            rest.ReportList = new ReportContainer()
            {
                PeriodStart = PStart,
                PeriodEnd = PEnd
            };

            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;
            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            return rest.ReportList;
        }

        public async Task<bool> SubmitReport(ReportContainer Reports)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Action = RestAPIUser.SubmitReport,
                ReportList = Reports,
                RequestingUser = this.User,
            };
            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;
            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            return rest.Success;
        }

        public async Task<bool> ChangePassword(string password)
        {
            UserRESTAPI rest = new UserRESTAPI()
            {
                Action = RestAPIUser.ChangePassword,
                RequestingUser = this.User,
                Password = password,
            };
            var json = JsonConvert.SerializeObject(rest);
            var data = new StringContent(json, Encoding.UTF8, "text/json");
            var client = new HttpClient();
            var response = await client.PostAsync(Conf.MakeServerAddress("api/User"), data);
            string result = response.Content.ReadAsStringAsync().Result;
            rest = JsonConvert.DeserializeObject<UserRESTAPI>(result);
            return rest.Success;
        }
    }
}
