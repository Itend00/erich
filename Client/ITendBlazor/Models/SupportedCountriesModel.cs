﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ItendSolution.Models
{
    public class SupportedCountriesModel
    {
        public List<string> Locale { get; set; }
        public List<string> Language { get; set; }
        public List<string> Country { get; set; }

        public SupportedCountriesModel()
        {
            Locale = new List<string>();
            Language = new List<string>();
            Country = new List<string>();
        }
    }
}