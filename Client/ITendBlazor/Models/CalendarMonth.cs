﻿
using System.Collections.Generic;

namespace ItendSolution.Models
{
    /// <summary>
    /// Data about local calendar in localized form
    /// </summary>
    public class CalendarMonth
    {
        /// <summary>
        /// Returns the value from the request.
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Returns the value from the request.
        /// </summary>
        public int Month { get; set; }
        /// <summary>
        /// Number of days in that specific month.
        /// </summary>
        public int NumberOfDays { get; set; }
        /// <summary>
        /// Month name in short form ex. Jan, Feb
        /// </summary>
        public string MonthShort { get; set; }
        /// <summary>
        /// Month name in long form ex. Janyary, February
        /// </summary>
        public string MonthLong { get; set; }
        /// <summary>
        /// First day of the month
        /// 1 = Monday
        /// 7 = Sunday
        /// </summary>
        public int FirstDay { get; set; }
        /// <summary>
        /// Day names in localized form [0] = Mon .... [6] = Sun 
        /// </summary>
        public string[] DayNamesShort { get; set; }
        /// <summary>
        /// Day names in localized form  [0] = Monday .... [6] = Sunday 
        /// </summary>
        public string[] DayNamesLong { get; set; }
        /// <summary>
        /// Array of booleans [32] 
        /// True is the day is non working day in calendar like, independence day.
        /// </summary>
        public string[] NonWorking { get; set; }

        public CalendarMonth()
        {
            this.DayNamesShort = new string[7];
            this.DayNamesLong = new string[7];
            this.NonWorking = new string[32];     // [0] = no such day in calendar, [1..31];
        }
    }
}