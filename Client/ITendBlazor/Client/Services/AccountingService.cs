﻿using ItendSolution.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ItendBlazor;

namespace ItendBlazor.Services
{
    public class AccountingService
    {
        public ReportRequest ReportRequest { get; set; }
        public DaySplit DaySplit { get; set; } 

        public AccountingService()
        {
            ReportRequest = new ReportRequest();
            DaySplit = new DaySplit();
        }

        public async Task WriteDaySplit()
        {
            var json = JsonConvert.SerializeObject(DaySplit);
            var data = new StringContent(json, Encoding.UTF8, "text/json");

            var client = new HttpClient();
            await client.PostAsync(Conf.MakeServerAddress("api/accounting/postdays"), data);
        }

        public async Task<DaySplit> GetSupportedCountries()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(Conf.MakeServerAddress("api/accounting/getdays"));
            string result = response.Content.ReadAsStringAsync().Result;
            this.DaySplit = JsonConvert.DeserializeObject<DaySplit>(result);
            return this.DaySplit;
        }
    }
}
