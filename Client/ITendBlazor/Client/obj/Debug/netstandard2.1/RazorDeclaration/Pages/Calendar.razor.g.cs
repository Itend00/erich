// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace ITendBlazor.Client.Pages
{
    #line hidden
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using ITendBlazor.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using ITendBlazor.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Blazored.Modal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Blazored.Modal.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\Calendar.razor"
using System;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\Calendar.razor"
using ItendSolution.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\Calendar.razor"
using ItendSolution.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\Calendar.razor"
using ItendBlazor.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\Calendar.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/Calendar")]
    public partial class Calendar : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 137 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\Calendar.razor"
      

    private bool loading = true;
    private MonthView View;
    private CalendarMonth C; // to shorten some lines
    private ReportContainer ReportList;
    private bool bShowManyDays = false;


    private async Task _initVars()
    {
        localeService.Add("title_company_paid_meals");
        localeService.Add("btn_businesstrip");
        localeService.Add("btn_prefill_workdays");
        localeService.Add("btn_clear_calendar");
        localeService.Add("btn_submit");
        localeService.Add("msg_please_select_days_from_calendar");
        localeService.Add("title_new_day_entry_for");
        localeService.Add("btn_add_period");
        localeService.Add("title_new_entry_for_period");
        localeService.AddList(RestAPIReport.NameList());
        await localeService.UpdateFromServerAsync();
        await calendarService.FetchCalendar(DateTime.Today.Year, DateTime.Today.Month, userService.Locale);
        C = calendarService.Month;
        MyDateTime s = new MyDateTime(C.Year, C.Month, 1);
        ReportList = await userService.FetchReport(s, s);
        View = new MonthView(C);
    }

    protected override async Task OnInitializedAsync()
    {
        if (loading)
        {
            await _initVars();
            loading = !loading;
        }
    }



    private async Task _changemonth(int delta)
    {
        C.Month += delta;
        if (C.Month<1)
        {
            C.Year--;
            C.Month = 12;
        }
        if (C.Month>12)
        {
            C.Year++;
            C.Month = 1;
        }
        await calendarService.FetchCalendar(C.Year,C.Month,userService.Locale);
        C = calendarService.Month;
        MyDateTime s = new MyDateTime(C.Year, C.Month, 1);
        ReportList = await userService.FetchReport(s,s);
        View = new MonthView(C);
    }

    private async Task OnNext()
    {
        await _changemonth(1);
        this.StateHasChanged();
    }

    private async Task OnPrev()
    {
        await _changemonth(-1);
        this.StateHasChanged();
    }

    // TODO handle failed request
    private async Task OnSubmit()
    {
        await userService.SubmitReport(ReportList);
    }

    private bool _SkipDay(ReportModel Date)
    {
        bool retval = false;
        bool bWorkingDay;

        bWorkingDay = userService.User.WorkModel.WorkingDays[Date.Start.WeekDay] > 0.0000001;

        if (!bWorkingDay)
        {
            switch (Date.Type)
            {
                case RestAPIReport.UnpaidOff: retval = true; break;
                case RestAPIReport.PaidOff: retval = true; break;
                case RestAPIReport.SickDay: retval = true; break;
                case RestAPIReport.Sickness: retval = true; break;
                case RestAPIReport.Holiday: retval = true; break;
                default: retval = false; break;
            }
        }
        return retval;
    }


    private async Task OnAddPeriod()
    {
        ReportModel ReportItem, tmp;
        ModalParameters Params = new ModalParameters();
        UserWorkModel wm = userService.User.WorkModel; // Just to shorten
        MyDateTime Start = new MyDateTime(C.Year, C.Month, View.SelectionStart, wm.WorkStarts.Hours, wm.WorkStarts.Minutes);
        MyDateTime End = new MyDateTime(C.Year, C.Month, View.SelectionEnd, wm.WorkEnds.Hours, wm.WorkEnds.Minutes);
        Params.Add("StartTime", Start);
        Params.Add("EndTime", End);

        var frmModal = Modal.Show<Client.Pages.ModalManyDays>(localeService.Get("title_new_entry_for_period") + ": " + View.SelectionStart.ToString() + "-" + View.SelectionEnd.ToString(), Params);
        var result = await frmModal.Result;

        if (!result.Cancelled)
        {
            ReportItem = (ReportModel)result.Data;

            int wd; // Weekday
            int hh, mm;

            mm = MyDateTime.MinuteDiff(wm.LunchStart, wm.LunchEnd);
            hh = MyDateTime.HourDiff(wm.LunchStart, wm.LunchEnd);

            // First Day
            if (!_SkipDay(ReportItem))
            {
                tmp = new ReportModel(ReportItem);
                wd = CalendarCalculator.WeekDay(tmp.Start.Year, tmp.Start.Month, tmp.Start.Day);
                tmp.End = new MyDateTime(C.Year, C.Month, ReportItem.Start.Day, wm.WorkEnds.Hours, wm.WorkEnds.Minutes);
                if (MyDateTime.fHourDiff(tmp.Start, tmp.End) > userService.User.WorkModel.WorkingDays[wd] - 0.0001)
                {
                    tmp.RemoveLunchHours(hh, mm);
                    tmp.FullDay = true;
                }
                ReportList.AddWithOverride(tmp);
            }

            // Last day
            tmp = new ReportModel(ReportItem);
            tmp.Start = new MyDateTime(C.Year, C.Month, ReportItem.End.Day, wm.WorkStarts.Hours, wm.WorkStarts.Minutes);
            if (!_SkipDay(tmp))
            {
                wd = CalendarCalculator.WeekDay(tmp.Start.Year, tmp.Start.Month, tmp.Start.Day);
                if (MyDateTime.fHourDiff(tmp.Start, tmp.End) > userService.User.WorkModel.WorkingDays[wd] - 0.0001)
                {
                    tmp.RemoveLunchHours(hh, mm);
                    tmp.FullDay = true;
                }
                ReportList.AddWithOverride(tmp);
            }

            int end = ReportItem.End.Day;
            int type = ReportItem.Type;
            for (int dind = ReportItem.Start.Day + 1; dind < end; dind++)
            {
                tmp = new ReportModel();
                tmp.Uid = userService.User.Id;
                tmp.Cid = userService.User.Cid;
                tmp.Type = ReportItem.Type;
                tmp.End.Year = tmp.Start.Year = C.Year;
                tmp.End.Month = tmp.Start.Month = C.Month;
                tmp.Start.Day = dind;
                tmp.Start.Hour = wm.WorkStarts.Hours;
                tmp.Start.Minute = wm.WorkStarts.Minutes;

                if (!_SkipDay(tmp))
                {
                    tmp.End.Day = dind;
                    tmp.End.Hour = wm.WorkEnds.Hours;
                    tmp.End.Minute = wm.WorkEnds.Minutes;
                    tmp.RemoveLunchHours(hh, mm);
                    tmp.FullDay = true;
                    ReportList.AddWithOverride(tmp);
                }
            }
        }
    }

    private void OnClick(MouseEventArgs e, int ind)
    {
        bShowManyDays = false;
        if (View.SelectionActive)
        {
            if (View.Day[ind].Selected)
            {
                View.SelectionRemove(C);
            }
            else
            {
                View.SelectionEnd = ind;
                View.Day[ind].Selected = true;
                View.SelectionRange(C);
                bShowManyDays = true;
            }
        }
        else
        {
            View.SelectionActive = true;
            View.Day[ind].Selected = true;
            View.SelectionEnd = View.SelectionStart = ind;
        }
    }

    private async Task OnDoubleClick(MouseEventArgs e, int ind)
    {
        MyDateTime ThisDay = new MyDateTime(C.Year, C.Month, ind);
        ReportModel ReportItem;
        var Params = new ModalParameters();
        Params.Add("ThisDay", ThisDay);
        var frmModal = Modal.Show<Client.Pages.ModalOneDay>(localeService.Get("title_new_day_entry_for") + " " + C.DayNamesLong[ThisDay.WeekDay] + ", " + ind.ToString(), Params);
        var result = await frmModal.Result;
        ReportItem = (ReportModel)result.Data;


        if (result.Cancelled == false)
        {
            if ((ReportItem.Type == RestAPIReport.Worktime) && (ReportItem.FullDay))
            {
                ReportModel rep0, rep1;
                UserWorkModel model = userService.User.WorkModel;
                rep0 = new ReportModel(userService.User);
                rep0.Start = new MyDateTime(C.Year, C.Month, ind, model.WorkStarts.Hours, model.WorkStarts.Minutes);
                rep0.End = new MyDateTime(C.Year, C.Month, ind, model.LunchStart.Hours, model.LunchStart.Minutes);
                rep0.Type = RestAPIReport.Worktime;
                ReportList.Add(rep0);

                // Lunch
                rep1 = new ReportModel(userService.User);
                rep1.Start = rep0.End;
                rep1.End = new MyDateTime(C.Year, C.Month, ind, model.LunchEnd.Hours, model.LunchEnd.Minutes);
                rep1.Type = RestAPIReport.Lunch;
                ReportList.Add(rep1);

                // Afternoon working
                rep0 = new ReportModel(userService.User);
                rep0.Start = rep1.End;
                rep0.End = new MyDateTime(C.Year, C.Month, ind, model.WorkEnds.Hours, model.WorkEnds.Minutes);
                rep0.Type = RestAPIReport.Worktime;
                ReportList.Add(rep0);
            }
            else
            {
                ReportList.Add(ReportItem);
            }

            ReportList.FixTimes();
        }
    }


    /// <summary>
    /// Prefills all working days for a month.
    ///
    /// TODO: Take public holidays into account
    /// </summary>
    private void ClickPrefill()
    {
        int ind = C.FirstDay;
        int i;

        UserWorkModel model = userService.User.WorkModel;
        ReportModel rep0, rep1;
        for (i = 1; i <= C.NumberOfDays; i++)
        {
            if (model.WorkingDays[ind] > 0.0f)
            {
                // Morning working
                rep0 = new ReportModel(userService.User);
                rep0.Start = new MyDateTime(C.Year, C.Month, i, model.WorkStarts.Hours, model.WorkStarts.Minutes);
                rep0.End = new MyDateTime(C.Year, C.Month, i, model.LunchStart.Hours, model.LunchStart.Minutes);
                rep0.Type = RestAPIReport.Worktime;
                ReportList.Add(rep0);

                // Lunch
                rep1 = new ReportModel(userService.User);
                rep1.Start = rep0.End;
                rep1.End = new MyDateTime(C.Year, C.Month, i, model.LunchEnd.Hours, model.LunchEnd.Minutes);
                rep1.Type = RestAPIReport.Lunch;
                ReportList.Add(rep1);

                // Afternoon working
                rep0 = new ReportModel(userService.User);
                rep0.Start = rep1.End;
                rep0.End = new MyDateTime(C.Year, C.Month, i, model.WorkEnds.Hours, model.WorkEnds.Minutes);
                rep0.Type = RestAPIReport.Worktime;
                ReportList.Add(rep0);

            }
            ind++;
            ind = (ind > 6) ? 0 : ind;
        }
    }

    private async Task ClickClear()
    {
        await _initVars();
        ReportList = new ReportContainer();
        this.StateHasChanged();
    }


    /// <summary>
    /// The view is ready for three more lines, but now make only one.
    /// </summary>
    /// <param name="day"></param>
    /// <param name="ind"></param>
    /// <returns>CSS class to be used with the grid item</returns>
    private string GetReportClass(int day, int ind)
    {
        string retval = "grid-item-sub ";

        //if (ind < 2)
        {

            switch (ReportList.GetDateFirst(day, userService.User.Id))
            {
                case RestAPIReport.Undefined: break;
                case RestAPIReport.Worktime: retval += "grid-item-work"; break;
                case RestAPIReport.UnpaidOff: retval += "grid-item-dayoff"; break;
                case RestAPIReport.PaidOff: retval += "grid-item-dayoff"; break;
                case RestAPIReport.SickDay: retval += "grid-item-holiday"; break;
                case RestAPIReport.Sickness: retval += "grid-item-illness"; break;
                case RestAPIReport.Holiday: retval += "grid-item-holiday"; break;
                case RestAPIReport.Lunch: break;
                case RestAPIReport.Overtime: retval += "grid-item-overtime"; break;
                case RestAPIReport.Oncall: retval += "grid-item-oncall"; break;
                default:
                    break;
            }
        }
        return retval;
    }


    public class DayView
    {

        // Group

        private bool WeekDay = false;
        private bool Saturday = false;
        private bool Sunday = false;
        private string PublicHoliday = null;

        public bool Selected = false;

        public DayView(bool NormalDay, bool Sat, bool Sun, string Pub)
        {
            this.WeekDay = NormalDay;
            this.Saturday = Sat;
            this.Sunday = Sun;
            this.PublicHoliday = Pub;
        }

        public string GetSelectedClass()
        {
            string ret = "grid-item ";
            if (this.Selected)
                ret = ret + "grid-item-selected";
            return ret;
        }

        public string GetClass(int ind)
        {
            string ret = null;

            if (this.Selected)
            {
                if (ind == 0)
                {
                    ret = "grid-item-header grid-item-selected";
                }
                else
                {
                    ret = "grid-item-daynumber grid-item-selected";
                }
            }
            else
            {
                if (ind == 0) ret = "grid-item-header "; else ret = "grid-item-daynumber ";
                if (this.PublicHoliday != null)
                {
                    ret = ret + "grid-item-holiday";
                }
                else
                {
                    {
                        if (this.Saturday || this.Sunday)
                        {
                            ret = ret + "grid-item-weekend";
                        }
                    }
                }
            }
            return ret;
        }

        public string GetDescription()
        {
            string ret = "";
            if (this.PublicHoliday != null)
            {
                ret = this.PublicHoliday;
            }
            return ret;
        }
    }

    private class MonthView
    {
        public List<DayView> Day;
        public bool SelectionActive = false;
        public int SelectionStart = 0;
        public int SelectionEnd = 0;

        public void SelectionRemove(CalendarMonth M)
        {
            int i;
            this.SelectionActive = false;
            this.SelectionStart = this.SelectionEnd = 0;
            for (i = 1; i <= M.NumberOfDays; i++)
            {
                this.Day[i].Selected = false;
            }
        }

        public void SelectionRange(int start, int end)
        {
            int i;
            this.SelectionActive = true;
            this.SelectionStart = start;
            this.SelectionEnd = end;
            for (i = start; i <= end; i++)
            {
                this.Day[i].Selected = true;
            }
        }

        public void SelectionRange(CalendarMonth M)
        {

            this.SelectionActive = true;

            int start = 1;
            int end = M.NumberOfDays;

            while (end > 0 && !this.Day[end].Selected) end--;
            while (start <= M.NumberOfDays && !this.Day[start].Selected) start++;

            this.SelectionStart = start;
            this.SelectionEnd = end;

            for (; start <= end; start++)
            {
                this.Day[start].Selected = true;
            }
        }

        public MonthView(CalendarMonth M)
        {
            int i;
            bool sa, su;

            Day = new List<DayView>(); // index 1..31
            Day.Add(null);
            for (i = 1; i < 32; i++)
            {
                sa = (((i + M.FirstDay + 1) % 7) == 0) ? true : false;
                su = (((i + M.FirstDay) % 7) == 0) ? true : false;
                Day.Add(new DayView(!(sa || su), sa, su, M.NonWorking[i]));
            }
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IModalService Modal { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private CalendarService calendarService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private UserService userService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private LocaleService localeService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration Configuration { get; set; }
    }
}
#pragma warning restore 1591
