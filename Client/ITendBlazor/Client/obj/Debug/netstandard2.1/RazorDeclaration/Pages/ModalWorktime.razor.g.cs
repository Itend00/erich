// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace ITendBlazor.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using ITendBlazor.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using ITendBlazor.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Blazored.Modal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Blazored.Modal.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalWorktime.razor"
using ItendBlazor.Services;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/worktime")]
    public partial class ModalWorktime : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 54 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalWorktime.razor"
       

    private bool loading = true;

    [CascadingParameter] BlazoredModalInstance BlazoredModal { get; set; }
    private void OnSubmit()
    {
        BlazoredModal.CloseAsync();
    }

    protected override async Task OnInitializedAsync()
    {
        if (loading)
        {
            localeService.Add("label_overtime_start_date");
            localeService.Add("label_overtime_start_time");
            localeService.Add("label_overtime_end_date");
            localeService.Add("label_overtime_end_time");
            localeService.Add("btn_submit");
            localeService.Add("btn_cancel");
            await localeService.UpdateFromServerAsync();
            loading = !true;
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private LocaleService localeService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
