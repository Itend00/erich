#pragma checksum "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4a67ea97e2bd01fbd03119a27c5024376a8b34bb"
// <auto-generated/>
#pragma warning disable 1591
namespace ITendBlazor.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using ITendBlazor.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using ITendBlazor.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Blazored.Modal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Blazored.Modal.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
using ItendBlazor.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
using ItendSolution.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
using ItendSolution.Shared;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/ModalOneDay")]
    public partial class ModalOneDay : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
#nullable restore
#line 9 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
 if (!Loading)
{

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(0, @"<style>
    .input-grid {
        display: grid;
        grid-template-columns: 1fr 3fr;
    }

    .input-grid-radio {
        display: grid;
        grid-template-columns: 1fr;
        border-right: 1px solid black;
        padding-right: 5px;
    }

    .input-radio-right {
        margin-left: 20px;
    }

    .label-grayed {
        color: gray;
    }

    .label-not-grayed {
        color: black;
    }
</style>
    ");
            __builder.OpenElement(1, "div");
            __builder.AddMarkupContent(2, "\r\n        ");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "input-grid");
            __builder.AddMarkupContent(5, "\r\n            ");
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "input-grid-radio");
            __builder.AddMarkupContent(8, "\r\n                ");
            __builder.OpenElement(9, "div");
            __builder.AddMarkupContent(10, "\r\n                    ");
            __builder.OpenElement(11, "input");
            __builder.AddAttribute(12, "type", "radio");
            __builder.AddAttribute(13, "id", "Worktime");
            __builder.AddAttribute(14, "value", "Worktime");
            __builder.AddAttribute(15, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 40 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                  OnChange

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(16, "checked", 
#nullable restore
#line 40 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                      Report.Type==RestAPIReport.Worktime

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\r\n                    ");
            __builder.OpenElement(18, "label");
            __builder.AddAttribute(19, "for", "Worktime");
            __builder.AddContent(20, 
#nullable restore
#line 41 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                           localeService.Get("label_day_type_worktime")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(21, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(22, "\r\n                ");
            __builder.OpenElement(23, "div");
            __builder.AddMarkupContent(24, "\r\n                    ");
            __builder.OpenElement(25, "input");
            __builder.AddAttribute(26, "type", "radio");
            __builder.AddAttribute(27, "id", "Overtime");
            __builder.AddAttribute(28, "value", "Overtime");
            __builder.AddAttribute(29, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 44 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                  OnChange

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(30, "checked", 
#nullable restore
#line 44 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                      Report.Type==RestAPIReport.Overtime

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n                    ");
            __builder.OpenElement(32, "label");
            __builder.AddAttribute(33, "for", "Overtime");
            __builder.AddContent(34, 
#nullable restore
#line 45 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                           localeService.Get("label_day_type_overtime")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(35, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(36, "\r\n                ");
            __builder.OpenElement(37, "div");
            __builder.AddMarkupContent(38, "\r\n                    ");
            __builder.OpenElement(39, "input");
            __builder.AddAttribute(40, "type", "radio");
            __builder.AddAttribute(41, "id", "Oncall");
            __builder.AddAttribute(42, "value", "Oncall");
            __builder.AddAttribute(43, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 48 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                              OnChange

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(44, "checked", 
#nullable restore
#line 48 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                  Report.Type==RestAPIReport.Oncall

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(45, "\r\n                    ");
            __builder.OpenElement(46, "label");
            __builder.AddAttribute(47, "for", "Oncall");
            __builder.AddContent(48, 
#nullable restore
#line 49 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                         localeService.Get("label_day_type_oncall")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(49, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(50, "\r\n                ");
            __builder.OpenElement(51, "div");
            __builder.AddMarkupContent(52, "\r\n                    ");
            __builder.OpenElement(53, "input");
            __builder.AddAttribute(54, "type", "radio");
            __builder.AddAttribute(55, "id", "Holiday");
            __builder.AddAttribute(56, "value", "Holiday");
            __builder.AddAttribute(57, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 52 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                OnChange

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(58, "checked", 
#nullable restore
#line 52 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                    Report.Type==RestAPIReport.Holiday

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(59, "\r\n                    ");
            __builder.OpenElement(60, "label");
            __builder.AddAttribute(61, "for", "Holiday");
            __builder.AddContent(62, 
#nullable restore
#line 53 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                          localeService.Get("label_day_type_holiday")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(63, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(64, "\r\n                ");
            __builder.OpenElement(65, "div");
            __builder.AddMarkupContent(66, "\r\n                    ");
            __builder.OpenElement(67, "input");
            __builder.AddAttribute(68, "type", "radio");
            __builder.AddAttribute(69, "id", "Sickness");
            __builder.AddAttribute(70, "value", "Sickness");
            __builder.AddAttribute(71, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 56 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                  OnChange

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(72, "checked", 
#nullable restore
#line 56 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                      Report.Type==RestAPIReport.Sickness

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(73, "\r\n                    ");
            __builder.OpenElement(74, "label");
            __builder.AddAttribute(75, "for", "Sickness");
            __builder.AddContent(76, 
#nullable restore
#line 57 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                           localeService.Get("label_day_type_sickness")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(77, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(78, "\r\n                ");
            __builder.OpenElement(79, "div");
            __builder.AddMarkupContent(80, "\r\n                    ");
            __builder.OpenElement(81, "input");
            __builder.AddAttribute(82, "type", "radio");
            __builder.AddAttribute(83, "id", "SickDay");
            __builder.AddAttribute(84, "value", "SickDay");
            __builder.AddAttribute(85, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 60 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                OnChange

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(86, "checked", 
#nullable restore
#line 60 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                    Report.Type==RestAPIReport.SickDay

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(87, "\r\n                    ");
            __builder.OpenElement(88, "label");
            __builder.AddAttribute(89, "for", "SickDay");
            __builder.AddContent(90, 
#nullable restore
#line 61 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                          localeService.Get("label_day_type_sickday")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(91, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(92, "\r\n                ");
            __builder.OpenElement(93, "div");
            __builder.AddMarkupContent(94, "\r\n                    ");
            __builder.OpenElement(95, "input");
            __builder.AddAttribute(96, "type", "radio");
            __builder.AddAttribute(97, "id", "PaidOff");
            __builder.AddAttribute(98, "value", "PaidOff");
            __builder.AddAttribute(99, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 64 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                OnChange

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(100, "checked", 
#nullable restore
#line 64 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                    Report.Type==RestAPIReport.PaidOff

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(101, "\r\n                    ");
            __builder.OpenElement(102, "label");
            __builder.AddAttribute(103, "for", "PaidOff");
            __builder.AddContent(104, 
#nullable restore
#line 65 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                          localeService.Get("label_day_type_paid_off")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(105, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(106, "\r\n                ");
            __builder.OpenElement(107, "div");
            __builder.AddMarkupContent(108, "\r\n                    ");
            __builder.OpenElement(109, "input");
            __builder.AddAttribute(110, "type", "radio");
            __builder.AddAttribute(111, "id", "UnpaidOff");
            __builder.AddAttribute(112, "value", "UnpaidOff");
            __builder.AddAttribute(113, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 68 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                    OnChange

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(114, "checked", 
#nullable restore
#line 68 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                        Report.Type==RestAPIReport.UnpaidOff

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(115, "\r\n                    ");
            __builder.OpenElement(116, "label");
            __builder.AddAttribute(117, "for", "UnpaidOff");
            __builder.AddContent(118, 
#nullable restore
#line 69 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                            localeService.Get("label_day_type_unpaid_off")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(119, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(120, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(121, "\r\n            ");
            __builder.OpenElement(122, "div");
            __builder.AddMarkupContent(123, "\r\n                ");
            __builder.OpenElement(124, "input");
            __builder.AddAttribute(125, "class", "input-radio-right");
            __builder.AddAttribute(126, "type", "radio");
            __builder.AddAttribute(127, "id", "FD");
            __builder.AddAttribute(128, "value", "FD");
            __builder.AddAttribute(129, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 73 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                             e=>OnClick(e,1)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(130, "checked", 
#nullable restore
#line 73 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                                         AMPMO == 1

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(131, "\r\n                ");
            __builder.OpenElement(132, "label");
            __builder.AddAttribute(133, "for", "FD");
            __builder.AddContent(134, 
#nullable restore
#line 74 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                 localeService.Get("label_full_day")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(135, "\r\n                ");
            __builder.OpenElement(136, "input");
            __builder.AddAttribute(137, "class", "input-radio-right");
            __builder.AddAttribute(138, "type", "radio");
            __builder.AddAttribute(139, "id", "AM");
            __builder.AddAttribute(140, "value", "AM");
            __builder.AddAttribute(141, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 75 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                             e=>OnClick(e,2)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(142, "checked", 
#nullable restore
#line 75 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                                         AMPMO == 2

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(143, "\r\n                ");
            __builder.OpenElement(144, "label");
            __builder.AddAttribute(145, "for", "AM");
            __builder.AddContent(146, 
#nullable restore
#line 76 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                 localeService.Get("label_AM")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(147, "\r\n                ");
            __builder.OpenElement(148, "input");
            __builder.AddAttribute(149, "class", "input-radio-right");
            __builder.AddAttribute(150, "type", "radio");
            __builder.AddAttribute(151, "id", "PM");
            __builder.AddAttribute(152, "value", "PM");
            __builder.AddAttribute(153, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 77 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                             e=>OnClick(e,3)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(154, "checked", 
#nullable restore
#line 77 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                                         AMPMO == 3

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(155, "\r\n                ");
            __builder.OpenElement(156, "label");
            __builder.AddAttribute(157, "for", "PM");
            __builder.AddContent(158, 
#nullable restore
#line 78 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                 localeService.Get("label_PM")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(159, "\r\n                ");
            __builder.OpenElement(160, "input");
            __builder.AddAttribute(161, "class", "input-radio-right");
            __builder.AddAttribute(162, "type", "radio");
            __builder.AddAttribute(163, "id", "Other");
            __builder.AddAttribute(164, "value", "Other");
            __builder.AddAttribute(165, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 79 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                   e=>OnClick(e,4)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(166, "checked", 
#nullable restore
#line 79 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                                               AMPMO == 4

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(167, "\r\n                ");
            __builder.OpenElement(168, "label");
            __builder.AddAttribute(169, "for", "Other");
            __builder.AddContent(170, 
#nullable restore
#line 80 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                    localeService.Get("label_Other")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(171, "\r\n                <br>\r\n                ");
            __builder.OpenElement(172, "input");
            __builder.AddAttribute(173, "class", "input-radio-right");
            __builder.AddAttribute(174, "type", "time");
            __builder.AddAttribute(175, "disabled", 
#nullable restore
#line 82 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                        IsDisabled

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(176, "id", "T1");
            __builder.AddAttribute(177, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 82 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                          BoxStartTime

#line default
#line hidden
#nullable disable
            , format: "HH:mm:ss", culture: global::System.Globalization.CultureInfo.InvariantCulture));
            __builder.AddAttribute(178, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => BoxStartTime = __value, BoxStartTime, format: "HH:mm:ss", culture: global::System.Globalization.CultureInfo.InvariantCulture));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(179, "\r\n                ");
            __builder.OpenElement(180, "label");
            __builder.AddAttribute(181, "class", 
#nullable restore
#line 83 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                               Grayed

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(182, "for", "T1");
            __builder.AddContent(183, 
#nullable restore
#line 83 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                 localeService.Get("label_start")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(184, "\r\n                ");
            __builder.OpenElement(185, "input");
            __builder.AddAttribute(186, "class", "input-radio-right");
            __builder.AddAttribute(187, "type", "time");
            __builder.AddAttribute(188, "disabled", 
#nullable restore
#line 84 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                        IsDisabled

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(189, "id", "T2");
            __builder.AddAttribute(190, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 84 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                                                                          BoxEndTime

#line default
#line hidden
#nullable disable
            , format: "HH:mm:ss", culture: global::System.Globalization.CultureInfo.InvariantCulture));
            __builder.AddAttribute(191, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => BoxEndTime = __value, BoxEndTime, format: "HH:mm:ss", culture: global::System.Globalization.CultureInfo.InvariantCulture));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(192, "\r\n                ");
            __builder.OpenElement(193, "label");
            __builder.AddAttribute(194, "class", 
#nullable restore
#line 85 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                               Grayed

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(195, "for", "T2");
            __builder.AddContent(196, 
#nullable restore
#line 85 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                 localeService.Get("label_end")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(197, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(198, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(199, "\r\n        <br>\r\n        ");
            __builder.OpenElement(200, "button");
            __builder.AddAttribute(201, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 89 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                          OnCancel

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(202, "class", "btn btn-secondary");
            __builder.AddContent(203, 
#nullable restore
#line 89 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                               localeService.Get("btn_cancel")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(204, "\r\n        ");
            __builder.OpenElement(205, "button");
            __builder.AddAttribute(206, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 90 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                          OnSubmit

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(207, "class", "btn btn-primary");
            __builder.AddContent(208, 
#nullable restore
#line 90 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
                                                             localeService.Get("btn_submit")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(209, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(210, "\r\n");
#nullable restore
#line 92 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
#nullable restore
#line 94 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalOneDay.razor"
       

    private bool Loading = true;
    protected bool IsDisabled = true;
    protected int AMPMO = 1;   // 1 = full day, 2 = AM, 3=PM, 4=Other
    protected string Grayed = "label-grayed";
    protected ReportModel Report;
    protected DateTime BoxStartTime;
    protected DateTime BoxEndTime;

    [CascadingParameter] BlazoredModalInstance BlazoredModal { get; set; }
    [ParameterAttribute] public MyDateTime ThisDay { get; set; }

    public void OnChange(ChangeEventArgs args)
    {
        switch (args.Value.ToString())
        {
            case "Worktime":
                Report.Type = RestAPIReport.Worktime;
                break;

            case "Holiday":
                Report.Type = RestAPIReport.Holiday;
                break;

            case "Sickness":
                Report.Type = RestAPIReport.Sickness;
                break;

            case "SickDay":
                Report.Type = RestAPIReport.SickDay;
                break;

            case "PaidOff":
                Report.Type = RestAPIReport.PaidOff;
                break;

            case "UnpaidOff":
                Report.Type = RestAPIReport.UnpaidOff;
                break;

            case "Overtime":
                Report.Type = RestAPIReport.Overtime;
                break;

            case "Oncall":
                Report.Type = RestAPIReport.Oncall;
                break;
        }
    }

    public void OnClick(MouseEventArgs args, int ind)
    {
        Grayed = "label-grayed";
        IsDisabled = true;
        if (ind != 1)
        {
            AMPMO = AMPMO == ind ? 1 : ind; // Full day becomes active if click twice same
        }
        else
        {
            AMPMO = 1;
        }
        if (AMPMO == 4)
        {
            Grayed = "label-not-grayed";
            IsDisabled = false;
        }
    }

    private void OnSubmit()
    {
        switch (AMPMO)
        {
            case 1:
                Report.FullDay = true; // <- This will also set start and end times therefore needs to be here
                if (Report.Type == RestAPIReport.Worktime)
                {
                    // This will be splitted into three different report items: AM, Lunch, PM
                    Report.Start = new MyDateTime(userService.User.WorkModel.WorkStarts);
                    Report.End = new MyDateTime(userService.User.WorkModel.WorkEnds);
                }
                break;
            case 2:
                Report.AM = true;
                break;
            case 3:
                Report.PM = true;
                break;
            case 4:
                Report.Start.Hour = BoxStartTime.Hour;
                Report.Start.Minute = BoxStartTime.Minute;
                Report.End.Hour = BoxEndTime.Hour;
                Report.End.Minute = BoxEndTime.Minute;
                Report.FullDay = false; // This clears also AM, PM...
                break;
        }

        Report.Start.Year = Report.End.Year = ThisDay.Year;
        Report.Start.Month = Report.End.Month = ThisDay.Month;
        Report.Start.Day = Report.End.Day = ThisDay.Day;

        BlazoredModal.CloseAsync(ModalResult.Ok<ReportModel>(Report));
    }

    private void OnCancel()
    {
        IsDisabled = !IsDisabled;
        Grayed = IsDisabled ? "label-grayed" : "label-not-grayed";
        BlazoredModal.CancelAsync();
    }

    protected override async Task OnInitializedAsync()
    {
        if (Loading)
        {
            int h = userService.User.WorkModel.WorkStarts.Hours;
            int m = userService.User.WorkModel.WorkStarts.Minutes;
            BoxStartTime = new DateTime(ThisDay.Year, ThisDay.Month, ThisDay.Day, h, m, 0);

            h = userService.User.WorkModel.WorkEnds.Hours;
            m = userService.User.WorkModel.WorkEnds.Minutes;
            BoxEndTime = new DateTime(ThisDay.Year, ThisDay.Month, ThisDay.Day, h, m, 0);

            Report = new ReportModel(userService.User);

            Report.Type = RestAPIReport.Worktime;
            Report.FullDay = true;
            localeService.Add("btn_cancel");
            localeService.Add("btn_submit");

            localeService.Add("label_day_type_worktime");
            localeService.Add("label_day_type_unpaid_off");
            localeService.Add("label_day_type_paid_off");
            localeService.Add("label_day_type_sickday");
            localeService.Add("label_day_type_sickness");
            localeService.Add("label_day_type_holiday");
            localeService.Add("label_day_type_overtime");
            localeService.Add("label_day_type_oncall");
            localeService.Add("label_full_day");
            localeService.Add("label_AM");
            localeService.Add("label_PM");
            localeService.Add("label_Other");
            localeService.Add("label_start");
            localeService.Add("label_end");
            await localeService.UpdateFromServerAsync();
            IsDisabled = true;
            Loading = !true;
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private UserService userService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private LocaleService localeService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
