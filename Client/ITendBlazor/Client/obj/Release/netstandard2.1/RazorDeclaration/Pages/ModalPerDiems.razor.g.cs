// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace ITendBlazor.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using ITendBlazor.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using ITendBlazor.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Blazored.Modal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\_Imports.razor"
using Blazored.Modal.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalPerDiems.razor"
using ItendBlazor.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalPerDiems.razor"
using ItendSolution.Models;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/perdiems")]
    public partial class ModalPerDiems : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 60 "C:\Devel\iTend\Erich\Client\ITendBlazor\Client\Pages\ModalPerDiems.razor"
 
    private bool loading = true;
    private MyDateTime start = new MyDateTime();
    private MyDateTime end = new MyDateTime();

    private void OnChange(MouseEventArgs args, int meal, int ind)
    {
        switch (meal)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }
    }

    [CascadingParameter] BlazoredModalInstance BlazoredModal { get; set; }
    private void OnSubmit()
    {
        BlazoredModal.CloseAsync();
    }

    protected override async Task OnInitializedAsync()
    {
        if (loading)
        {

            int i;
            int diff = MyDateTime.DayDiff(start, end);

            for (i = 0; i <= diff; i++)
            {
            }

            localeService.Add("label_breakfast");
            localeService.Add("label_lunch");
            localeService.Add("label_dinner");
            localeService.Add("btn_submit");
            await localeService.UpdateFromServerAsync();
            loading = !true;
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private CalendarService calendarService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private LocaleService localeService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
