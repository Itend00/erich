﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItendBlazor
{
    static public class Conf
    {
        static string ServerAddr = "https://devitendwebapi.azurewebsites.net/";
        //static readonly string ServerAddr = "https://itendapi.com/";
        // static readonly string ServerAddr = "https://localhost:44395/";

        public static string MakeServerAddress(string str)
        {
            return ServerAddr + str;
        }
    }
}
