﻿using System;
using System.Collections.Generic;

namespace ItendSolution.Models
{
    /// <summary>
    /// Use to fetch locales for a supported language
    /// </summary>
    public class LocaleModel
    {
        /// <summary>
        /// Language to which translate: EN, CZ, FI, ....
        /// </summary>
        public string Lang { get; set; }
        /// <summary>
        /// Input: List of locales to be fetched
        /// </summary>
        public List<string> Locale { get; set; }

        /// <summary>
        /// Returned localized string
        /// </summary>
        public List<string> Translation { get; set; }

        /// <summary>
        /// Initialize objects and set the language to EN.
        /// </summary>
        public LocaleModel()
        {
            this.Lang = "EN"; // Default
            this.Locale = new List<string>();
            this.Translation = new List<string>();
        }

        public LocaleModel(string lang)
        {
            this.Lang = lang;
            this.Locale = new List<string>();
            this.Translation = new List<string>();
        }

        public void Clear()
        {
            this.Locale.Clear();
            this.Translation.Clear();
        }

        /// <summary>
        /// Return translated locale.
        /// This assumes that the local has already been fetched from the server.
        /// </summary>
        /// <param name="Loc">Locale Example: "btn_vacation" </param>
        /// <returns>Translation</returns>
        public string Get(string Loc)
        {
            int ind;
            string ret = Loc; // in case there is an error in code, we see what was requested.
            try
            {
                ind = this.Locale.IndexOf(Loc);
                if (ind > -1)
                {
                    ret = this.Translation[ind];
                }
            }
            catch (Exception e)
            {
                ret = "LocaleModel: Get "+e.Message;
            }
            return ret;
        }
        /// <summary>
        /// Adds a locale to list for sending to the server.
        /// </summary>
        /// <param name="Loc">Locale exmple: btn_submit</param>
        public void AddLocale(string Loc)
        {
            if (this.Locale.IndexOf(Loc) == -1)
            {
                this.Locale.Add(Loc);
                this.Translation.Add(Loc); // If not found in DB, we see what is missing see AddTranslated.
            }
        }
        
        /// <summary>
        /// Ussually the Server adds translations from DB using this function.
        /// But of course can be used by anyone.
        /// The date in this.Locale received from client will be kept as is.
        /// </summary>
        /// <param name="Loc">Locale example: label_tripenddate</param>
        /// <param name="Trans">Translated locale.</param>
        public void AddTranslated(string Loc, string Trans)
        {
            int ind;
            ind = this.Locale.IndexOf(Loc);
            if (ind>-1)
            {
                this.Translation[ind] = Trans;
            }
        }

        public void Merge(LocaleModel L)
        {
            int i;
            for(i=0;i<L.Locale.Count;i++)
            {
                this.AddTranslated(L.Locale[i], L.Translation[i]);
            }
        }
    }
}