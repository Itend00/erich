using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Blazored.Modal;
using ItendBlazor.Services;

namespace ITendBlazor.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");
            builder.Services.AddTransient(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddBlazoredModal();
            builder.Services.AddSingleton<UserService>();
            builder.Services.AddSingleton<LocaleService>();
            builder.Services.AddSingleton<RefreshService>();
            builder.Services.AddSingleton<CalendarService>();
            builder.Services.AddSingleton<AccountingService>();

            var host = builder.Build();
            var userService = host.Services.GetRequiredService<UserService>();
            var localeService = host.Services.GetRequiredService<LocaleService>();
            var refreshService = host.Services.GetRequiredService<RefreshService>();
            var calendarService = host.Services.GetRequiredService<CalendarService>();
            var accountingService = host.Services.GetRequiredService<AccountingService>();

            var client = new HttpClient()
            {
                BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)
            };

            builder.Services.AddTransient(sp => client);
            var response = await client.GetAsync("config.json");
            var stream = await response.Content.ReadAsStreamAsync();
            builder.Configuration.AddJsonStream(stream);
            await builder.Build().RunAsync();
        }
    }
}
