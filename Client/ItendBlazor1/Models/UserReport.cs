﻿using System;
using System.Collections.Generic;
using ItendSolution.Shared;

namespace ItendSolution.Models
{
    public class ReportModel
    {
        public int Uid { get; set; }             // User ID;
        public int Cid { get; set; }             // Company ID
        public MyDateTime Start { get; set; }
        public MyDateTime End { get; set; }
        public int _Type { get; set; }           // RestAPIReport in <SERVER>SharedDefinitions
        public String Description { get; set; }

        private bool _AM;
        private bool _PM;
        private bool _FullDay;
        private UserModel _User;              // TODO make the system user u

        public ReportModel(UserModel user)
        {
            this._User = user;
            this.Uid = user.Id;
            this.Cid = user.Cid;             // Company ID
            this.Start = new MyDateTime();
            this.End = new MyDateTime();
            this.Description = "";

            this._AM = false;
            this._PM = false;
            this._FullDay = false;
            this._Type = RestAPIReport.Undefined;           // RestAPIReport in <SERVER>SharedDefinitions
        }

        public ReportModel(ReportModel r)
        {
            this._User = r._User;
            this.Uid = r.Uid;
            this.Cid = r.Cid;             // Company ID
            this.Start = new MyDateTime(r.Start);
            this.End = new MyDateTime(r.End);
            this.Description = "";

            this._AM = false;
            this._PM = false;
            this._FullDay = false;
            this._Type = r.Type;           // RestAPIReport in <SERVER>SharedDefinitions
        }
        /// <summary>
        /// Used in server to return report list. 
        /// </summary>
        public ReportModel()
        {

            this.Uid = 0;
            this.Cid = 0;
            this.Start = new MyDateTime();
            this.End = new MyDateTime();
            this.Description = "";

            this._AM = false;
            this._PM = false;
            this._FullDay = false;
            this._Type = RestAPIReport.Undefined;           // RestAPIReport in <SERVER>SharedDefinitions
        }

        public String Name
        {
            get
            {
                return RestAPIReport.Name(this._Type);
            }
        }

        public int Type
        {
            get { return this._Type; }
            set { this._Type = value; }
        }


        // TODO: Check if half day is until or after Lunch hours....
        // Maybe even need to make this configurable.
        public bool AM
        {
            get { return this._AM; }
            set
            {
                if (value)
                {
                    this.Start.SetTime(this._User.WorkModel.WorkStarts);
                    this.End.SetTime(this._User.WorkModel.LunchStart);
                }
                this._AM = value;
                this._PM = false;
                this._FullDay = false;
            }
        }

        public bool PM
        {
            get { return this._PM; }
            set
            {
                if (value)
                {
                    this.Start.SetTime(this._User.WorkModel.LunchEnd);
                    this.End.SetTime(this._User.WorkModel.WorkEnds);
                }
                this._AM = false;
                this._PM = value;
                this._FullDay = false;
            }
        }

        public void RemoveLunchHours(int hh,int mm)
        {
            MyDateTime tmp = new MyDateTime(this.End);
            this.End.DecMinutes(mm);
            this.End.DecHours(hh);
            if (this.End<=this.Start)
            {
                this.End = tmp;
            }
        }

        public bool FullDay
        {
            get { return this._FullDay; }
            set 
                {
                    this._AM = false;
                    this._PM = false;
                    this._FullDay = value;
                }
        }
        
        public string StrDeltaTime
        {
           get 
           {
                return MyDateTime.StrHourDiff(this.Start, this.End);
           }
        }

        public float DeltaTime
        {
            get
            {
                return MyDateTime.HourDiff(this.Start, this.End);
            }
        }

        /// <summary>
        /// Return how much time was spend between start and end
        /// </summary>
        /// <returns>MydateTime: Days, Hours and Minutes </returns>
        public MyDateTime TimeSpend()
        {
            float h = MyDateTime.HourDiff(this.Start, this.End);
            MyDateTime spend = new MyDateTime(this.Start)
            {
                Hour = (int)h,
                Minute = (int)((h-((int)h)) * 60.0),
            };
            return spend;
        }
    }

    public class ReportContainer
    {
        public List<ReportModel> ReportList { get; set; }
        public MyDateTime PeriodStart { get; set; }
        public MyDateTime PeriodEnd { get; set; }

        private int _datecounter; /* for GetDateFirstWork, GetDateNextWork */

        public ReportContainer()
        {
            this.ReportList = new List<ReportModel>();
            this._datecounter = 0;
        }

        /// <summary>
        /// Get the first marking for a day.
        /// Assumed here that the markings for each day are in ordered list
        /// </summary>
        /// <param name="d">Date to fetch</param>
        /// <returns>RestAPIReport.<type></returns>
        public int GetDateFirst(int Day, int UiD)
        {
            int i, retval;
            bool bfound = false;
            ReportModel tmp;
            retval = RestAPIReport.Undefined;
            i = 0;
            while ((i<ReportList.Count) && (!bfound))
            {
                tmp = ReportList[i];
                if ((tmp.Uid == UiD) && (tmp.Start.Day == Day))
                {
                    bfound = true;
                    retval = tmp.Type;
                }
                i++;
            }
            return retval;
        }

        public ReportModel GetDateFirstEntry(int Day, int UiD)
        {
            int i;
            bool bfound = false;
            ReportModel retval = null;

            i = 0;
            _datecounter = 0;
            while ((i < ReportList.Count) && (!bfound))
            {
                if ((ReportList[i].Uid == UiD) && (ReportList[i].Start.Day == Day))
                {
                    bfound = true;
                    retval = ReportList[i];
                    _datecounter = i + 1;
                }
                i++;
            }
            return retval;
        }

        public ReportModel GetDateNextEntry(int Day, int UiD)
        {
            int i = _datecounter;
            bool bfound = false;
            ReportModel retval = null;

            while ((i < ReportList.Count) && (!bfound))
            {
                if ((ReportList[i].Uid == UiD) && (ReportList[i].Start.Day == Day))
                {
                    bfound = true;
                    retval = ReportList[i];
                    _datecounter = i + 1;
                }
                i++;
            }

            return retval;
        }

        public int ReportCompare(ReportModel r1, ReportModel r2)
        {
            int comp = r1.Start.Day - r2.Start.Day;
            if (comp==0)
            {
                comp = r1.Start.Hour - r2.Start.Hour;
                if (comp==0)
                {
                    comp = r1.Start.Minute - r2.Start.Minute;
                    if (comp==0)
                    {
                        comp = r1.Type - r2.Type;
                    }
                }
            }
            return comp;
        }
        
        /// <summary>
        /// Count the number of different Users in the list.
        /// Server side operation
        /// </summary>
        public List<int> ListUid()
        {
            List<int> retval = new List<int>();
            int i;
            for(i=0;i<this.ReportList.Count;i++)
            {
                if (!retval.Exists(x => x == this.ReportList[i].Uid))
                {
                    retval.Add(this.ReportList[i].Uid);
                }
            }
            return retval;
        }
         

        public void Add(ReportModel rep)
        {
            this.ReportList.Add(rep);
            this.ReportList.Sort(ReportCompare);
        }

        public void AddWithOverride(ReportModel rep)
        {
            int start, end, ind;
            start = 0;
            while ((start<ReportList.Count) && (ReportList[start].Start.Day != rep.Start.Day)) start++;
            end = start;
            while ((end<ReportList.Count) && (ReportList[end].Start.Day == rep.Start.Day)) end++;
            ind = start;
            while ((end < ReportList.Count) && (ind<end))
            {
                if (ReportList[ind].Start < rep.Start)
                {
                    ReportList[ind].End = rep.Start;
                }
                else
                {
                    if (ReportList[ind].End <= rep.End)
                    {
                        ReportList.RemoveAt(ind);
                        ind--;
                        end--;
                    }
                    else
                    {
                        if (rep.FullDay)
                        {
                            ReportList.RemoveAt(ind);
                            ind--;
                            end--;
                        }
                        else
                        {
                            ReportList[ind].Start = new MyDateTime(rep.End);
                        }
                    }
                }
                ind++;
                while ((end < ReportList.Count) && (ReportList[end].Start.Day == rep.Start.Day)) end++;
            }
            Add(rep);
        }

        public void Sort()
        {
            this.ReportList.Sort(ReportCompare);
        }
        public int Count
        {
            get { return this.ReportList.Count; }
        }

        public ReportModel this[int index]
        {
            get { return ReportList[index]; }
            set { ReportList[index] = value; }
        }
        /// <summary>
        /// Removes entries for one day
        /// </summary>
        /// <param name="day"></param>
        public void ClearDay(int day)
        {
            int i=0;
            while (i<ReportList.Count)
            {
                while ((i < ReportList.Count) && (ReportList[i].Start.Day == day))
                {
                    ReportList.RemoveAt(i);
                }
                i++;
            }
        }
        /// <summary>
        /// If the times are overlapping we fix the times
        /// It is assumed that the list is already sorted.
        /// </summary>
        public void FixTimes()
        {
            bool working = true;
            int i, j;

            while (working)
            {
                working = false;
                for(i=0;i<this.Count-1;i++)
                {
                    for(j=i+1;j<this.Count;j++)
                    {
                        if (this.ReportList[i].End>this.ReportList[j].Start)
                        {
                            working = true;
                            this.ReportList[i].End = new MyDateTime(this.ReportList[j].Start);
                        }
                    }
                }
            }
        }
    }
}