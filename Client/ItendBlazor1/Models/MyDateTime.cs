﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ItendSolution.Shared;

#pragma warning disable IDE1006  // Do not like Code Polices..

namespace ItendSolution.Models
{
    public class MyDateTime
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }

        /// <summary>
        /// 0 = Monday, 6 = Sunday
        /// </summary>
        public int WeekDay
        {
            get
            {
                return CalendarCalculator.WeekDay(Year, Month, Day);
            }
        }

        public MyDateTime()
        {
            // Date of creation 
            this.Year = this.Month = this.Day = this.Hour = this.Minute = 0;
        }

        public MyDateTime(MyDateTime MyTime) // Create a copy;
        {
            this.Year = MyTime.Year;
            this.Month = MyTime.Month;
            this.Day = MyTime.Day;
            this.Hour = MyTime.Hour;
            this.Minute = MyTime.Minute;
        }

        public MyDateTime(int year, int month, int day)
        {
            this.Year = year;
            this.Month = month;
            this.Day = day;
            this.Hour = this.Minute = 0;
        }
        public MyDateTime(int year, int month, int day, int hour, int minute)
        {
            this.Year = year;
            this.Month = month;
            this.Day = day;
            this.Hour = hour;
            this.Minute = minute;
        }

        public MyDateTime(TimeSpan span)
        {
            DateTime dt = new DateTime(span.Ticks);
            this.Year = dt.Year;
            this.Month = dt.Month;
            this.Day = dt.Year;
            this.Hour = dt.Hour;
            this.Minute = dt.Minute;
        }

        public override string ToString() 
        {
            string retval;
            retval = this.Day.ToString() + "." + this.Month.ToString() + "." + this.Year.ToString() + " ";
            retval += this.ToStringTime();
            return retval;
        }

        /// <summary>
        /// Designed for Blazored Modal dialog return format
        /// </summary>
        /// <param name="MyDate">Date format "2020-04-20" or any "YYYY[char]MM[char]DD</param>
        /// <param name="MyTime">Time format "18-30" or any "HH[char]MM" <-24 hour format</param>
        public void Parse(string MyDate, string MyTime)
        {
            this.Year = int.Parse(MyDate.Substring(0, 4));
            this.Month = int.Parse(MyDate.Substring(5, 2));
            this.Day = int.Parse(MyDate.Substring(8, 2));
            this.Hour = int.Parse(MyTime.Substring(0, 2));
            this.Minute = int.Parse(MyTime.Substring(3, 2));
        }

        public string GetDateString(string separator)
        {
            string retval="";
            string tmp;
            tmp = "0"+this.Day.ToString();
            retval = retval + tmp.Substring(tmp.Length - 2, 2) + separator;
            tmp = "0" + this.Month.ToString();
            retval += tmp.Substring(tmp.Length - 2, 2) + separator;
            retval += this.Year.ToString();
            return retval;
        }

        public DateTime GetDateTime()
        {
            return new DateTime(this.Year, this.Month, this.Day, this.Hour, this.Minute, 0);            
        }
        public DateTime GetDate()
        {
            return new DateTime(this.Year, this.Month, this.Day, 0, 0, 0);
        }
        public DateTime GetTime()
        {
            return new DateTime(this.Year, this.Month, this.Day, this.Hour, this.Minute, 0);
        }

        public void SetTimeFromString(string str)
        {
            this.Hour = int.Parse(str.Substring(0, 2));
            this.Minute = int.Parse(str.Substring(3, 2));
        }

        public string MyTimeGet(string separator)
        {
            string retval, tmp;
            tmp = "0" + this.Hour.ToString();
            retval = tmp.Substring(tmp.Length - 2, 2) + separator;
            tmp = "0" + this.Minute.ToString();
            retval += tmp;
            return retval;
        }
        public void AddMinutes(int minutes)
        {
            DateTime Current = this.GetDateTime();
            Current = Current.AddMinutes(((double)minutes));
            this.Year = Current.Year;
            this.Month = Current.Month;
            this.Day = Current.Day;
            this.Hour = Current.Hour;
            this.Minute = Current.Minute;
        }
        /// <summary>
        /// Add days to the current date in class.
        /// </summary>
        /// <param name="days">Number of days to add</param>
        public void AddDays(int days)
        {
            DateTime Current = this.GetDate();
            Current = Current.AddDays(((double)days));
            this.Year = Current.Year;
            this.Month = Current.Month;
            this.Day = Current.Day;
        }

       
        // TODO Handle if crossing midnight
        public void DecHours(int hours)
        {
            this.Hour -= hours;
        }

        // TODO Handle if crossing midnight
        public void DecMinutes(int minutes)
        {
            int res = this.Minute - minutes;
            if (res<0)
            {
                this.Hour--;
                res += 60;
            }
            this.Minute = res;
        }

        /// <summary>
        /// Calculates differece in days (e-s)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <returns>Int number of days between two dates</returns>
        public static int DayDiff(MyDateTime start, MyDateTime end)
        {
            DateTime s = start.GetDate();
            DateTime e = end.GetDate();
            return ((int)(e - s).TotalDays);
        }

        /// <summary>
        /// Calcudates the difference in hours
        /// </summary>
        /// <returns>String: in format HH:MM</returns>
        public static string StrHourDiff(MyDateTime start, MyDateTime end)
        {
            DateTime s = start.GetTime();
            DateTime e = end.GetTime();
            TimeSpan delta = e - s;            
            MyDateTime temp = new MyDateTime(2020, 10, 10, delta.Hours, delta.Minutes);
            return temp.ToStringTime();  
        }

        /// <summary>
        /// Calcudates the difference in hours
        /// </summary>
        /// <returns>int: Difference in hours.</returns>
        public static float HourDiff(MyDateTime start, MyDateTime end)
        {
            DateTime s = start.GetTime();
            DateTime e = end.GetTime();            
            return (e - s).Hours+(e-s).Minutes/60.0f;
        }

        public static int HourDiff(TimeSpan start, TimeSpan end)
        {
            DateTime s = new DateTime(start.Ticks);
            DateTime e = new DateTime(end.Ticks);
            return (e - s).Hours;
        }

        public static int MinuteDiff(MyDateTime start, MyDateTime end)
        {
            DateTime s = start.GetTime();
            DateTime e = end.GetTime();
            return (e - s).Minutes;
        }

        public static int MinuteDiff(TimeSpan start, TimeSpan end)
        {
            DateTime s = new DateTime(start.Ticks);
            DateTime e = new DateTime(end.Ticks);
            return (e - s).Minutes;
        }

        public static float fHourDiff(MyDateTime start, MyDateTime end)
        {
            DateTime s = start.GetTime();
            DateTime e = end.GetTime();
            TimeSpan tmElapsed = e - s;
            return ((float)tmElapsed.TotalMinutes) / 60.0f;
        }

        public string ToStringTime()
        {
            string strh = "0" + this.Hour.ToString();
            strh = strh.Substring(strh.Length - 2, 2);
            string strm = "0" + this.Minute.ToString();
            strm = strm.Substring(strh.Length - 2, 2);
            return strh+":"+strm;
        }

        public string ToStringDate()
        {
            string strd = "0" + this.Day.ToString();
            string strm = this.Month.ToString();
            strd = strd.Substring(strd.Length - 2, 2);
            strm = strm.Substring(strm.Length - 2, 2);
            this.Year.ToString();
            return strd+"."+strm+"."+this.Year.ToString();
        }

        public void SetTime(TimeSpan Time)
        {
            this.Hour = Time.Hours;
            this.Minute = Time.Minutes;
        }

        public static bool operator >=(MyDateTime A, MyDateTime B)
        {
            if (A.Year > B.Year) return true;
            if (B.Year < B.Year) return false;
            if (A.Month > B.Month) return true;
            if (A.Month < B.Month) return false;
            if (A.Day > B.Day) return true;
            if (A.Day < B.Day) return false;
            if (A.Hour > B.Hour) return true;
            if (A.Hour < B.Hour) return false;
            if (A.Minute > B.Minute) return true;
            if (A.Minute < B.Minute) return false;
            return true;
        }

        public static bool operator <=(MyDateTime A, MyDateTime B)
        {
            if (A.Year > B.Year) return false;
            if (B.Year < B.Year) return true;
            if (A.Month > B.Month) return false;
            if (A.Month < B.Month) return true;
            if (A.Day > B.Day) return false;
            if (A.Day < B.Day) return true;
            if (A.Hour > B.Hour) return false;
            if (A.Hour < B.Hour) return true;
            if (A.Minute > B.Minute) return false;
            if (A.Minute < B.Minute) return true;
            return true;
        }

        public static bool operator >(MyDateTime A, MyDateTime B)
        {
            return !(A <= B);
        }

        public static bool operator <(MyDateTime A, MyDateTime B)
        {
            return !(A >= B);
        }

        public static bool operator ==(MyDateTime A, MyDateTime B)
        {
            int C = Math.Abs(A.Year - B.Year) + Math.Abs(A.Month - B.Month) + Math.Abs(A.Day - B.Day) + Math.Abs(A.Hour - B.Hour) + Math.Abs(A.Minute - B.Minute);
            return (C == 0);
        }

        public static bool operator !=(MyDateTime A, MyDateTime B)
        {
            return !(A == B);
        }
    }
}
