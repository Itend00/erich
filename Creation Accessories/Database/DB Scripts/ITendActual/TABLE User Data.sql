USE [ITendActual]
GO

/****** Object:  Table [dbo].[UserData]    Script Date: 09/10/2020 14:51:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Cid] [int] NOT NULL,
	[Company Division] [nvarchar](50) NULL,
	[Verified] [int] NOT NULL,
	[Inactive] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Alias] [nchar](50) NOT NULL,
	[Email] [nchar](128) NOT NULL,
	[CompanyId] [nchar](20) NULL,
	[IsAdmin] [int] NOT NULL,
	[IsAccounting] [int] NOT NULL,
	[IsUser] [int] NOT NULL,
	[ExtendedPermissions] [int] NOT NULL,
	[Vacation] [real] NOT NULL,
	[Sickdays] [real] NOT NULL,
	[Locale] [char](2) NOT NULL,
	[WorkModel] [int] NOT NULL,
	[Working Country] [nchar](100) NULL,
	[Per Diems] [int] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserData] ADD  CONSTRAINT [DF_User_UserId]  DEFAULT (newid()) FOR [UserId]
GO

ALTER TABLE [dbo].[UserData]  WITH CHECK ADD  CONSTRAINT [FK_WorkModel_UserData] FOREIGN KEY([WorkModel])
REFERENCES [dbo].[WorkModel] ([Id])
GO

ALTER TABLE [dbo].[UserData] CHECK CONSTRAINT [FK_WorkModel_UserData]
GO

