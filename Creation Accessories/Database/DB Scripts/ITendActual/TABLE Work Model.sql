USE [ITendActual]
GO

/****** Object:  Table [dbo].[WorkModel]    Script Date: 09/10/2020 14:51:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WorkModel](
	[Id] [int] NOT NULL,
	[Cid] [int] NOT NULL,
	[Name] [nchar](50) NOT NULL,
	[WorkStarts] [time](7) NOT NULL,
	[WorkEnds] [time](7) NOT NULL,
	[LunchStarts] [time](7) NOT NULL,
	[LunchEnds] [time](7) NOT NULL,
	[Mon] [real] NOT NULL,
	[Tue] [real] NOT NULL,
	[Wed] [real] NOT NULL,
	[Thu] [real] NOT NULL,
	[Fri] [real] NOT NULL,
	[Sat] [real] NOT NULL,
	[Sun] [real] NOT NULL,
 CONSTRAINT [PK_Workmodel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

