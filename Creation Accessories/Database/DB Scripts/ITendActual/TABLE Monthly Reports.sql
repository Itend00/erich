USE [ITendActual]
GO

/****** Object:  Table [dbo].[MonthlyReports]    Script Date: 09/10/2020 14:50:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MonthlyReports](
	[DiD] [int] IDENTITY(1,1) NOT NULL,
	[Id] [int] NOT NULL,
	[Cid] [int] NOT NULL,
	[Company Division] [nvarchar](50) NULL,
	[Year] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Object] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_MonthlyReports] PRIMARY KEY CLUSTERED 
(
	[DiD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[MonthlyReports]  WITH CHECK ADD  CONSTRAINT [FK_UserData_MonthlyReports] FOREIGN KEY([Id])
REFERENCES [dbo].[UserData] ([Id])
GO

ALTER TABLE [dbo].[MonthlyReports] CHECK CONSTRAINT [FK_UserData_MonthlyReports]
GO

