USE [ITend]
GO

/****** Object:  Table [dbo].[LocaleList]    Script Date: 09/10/2020 14:53:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LocaleList](
	[Locale] [char](2) NULL,
	[Language] [nchar](100) NULL,
	[Country] [nchar](100) NULL
) ON [PRIMARY]
GO

