USE [ITend]
GO

/****** Object:  Table [dbo].[IBAN]    Script Date: 09/10/2020 14:53:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IBAN](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Alpha-2] [nvarchar](50) NOT NULL,
	[Alpha-3] [nvarchar](50) NOT NULL,
	[Number] [nchar](3) NOT NULL
) ON [PRIMARY]
GO

