USE [ITend]
GO

/****** Object:  Table [dbo].[LocaleCalendar]    Script Date: 09/10/2020 14:53:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LocaleCalendar](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Locale] [nvarchar](50) NULL,
	[EN] [nvarchar](50) NULL,
	[EN_S] [nvarchar](10) NULL,
	[CZ] [nvarchar](50) NULL,
	[CZ_S] [nvarchar](10) NULL,
	[FI] [nvarchar](50) NULL,
	[FI_S] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

