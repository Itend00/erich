USE [ITend]
GO

/****** Object:  Table [dbo].[PublicHolidays]    Script Date: 09/10/2020 14:53:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PublicHolidays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NOT NULL,
	[Year] [smallint] NOT NULL,
	[Month] [smallint] NOT NULL,
	[Day] [smallint] NOT NULL,
	[Locale] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO

