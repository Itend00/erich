1,Afghanistan,L_AFG
2,Albania,L_ALB
3,Algeria,L_DZA
4,American Samoa,L_ASM
5,Andorra,L_AND
6,Angola,L_AGO
7,Anguilla,L_AIA
8,Antarctica,L_ATA
9,Antigua and Barbuda,L_ATG
10,Argentina,L_ARG
11,Armenia,L_ARM
12,Aruba,L_ABW
13,Australia,L_AUS
14,Austria,L_AUT
15,Azerbaijan,L_AZE
16,Bahamas (the),L_BHS
17,Bahrain,L_BHR
18,Bangladesh,L_BGD
19,Barbados,L_BRB
20,Belarus,L_BLR
21,Belgium,L_BEL
22,Belize,L_BLZ
23,Benin,L_BEN
24,Bermuda,L_BMU
25,Bhutan,L_BTN
26,Bolivia (Plurinational State of),L_BOL
27,"Bonaire, Sint Eustatius and Saba",L_BES
28,Bosnia and Herzegovina,L_BIH
29,Botswana,L_BWA
30,Bouvet Island,L_BVT
31,Brazil,L_BRA
32,British Indian Ocean Territory (the),L_IOT
33,Brunei Darussalam,L_BRN
34,Bulgaria,L_BGR
35,Burkina Faso,L_BFA
36,Burundi,L_BDI
37,Cabo Verde,L_CPV
38,Cambodia,L_KHM
39,Cameroon,L_CMR
40,Canada,L_CAN
41,Cayman Islands (the),L_CYM
42,Central African Republic (the),L_CAF
43,Chad,L_TCD
44,Chile,L_CHL
45,China,L_CHN
46,Christmas Island,L_CXR
47,Cocos (Keeling) Islands (the),L_CCK
48,Colombia,L_COL
49,Comoros (the),L_COM
50,Congo (the Democratic Republic of the),L_COD
51,Congo (the),L_COG
52,Cook Islands (the),L_COK
53,Costa Rica,L_CRI
54,Croatia,L_HRV
55,Cuba,L_CUB
56,Cura�ao,L_CUW
57,Cyprus,L_CYP
58,Czechia,L_CZE
59,C�te d'Ivoire,L_CIV
60,Denmark,L_DNK
61,Djibouti,L_DJI
62,Dominica,L_DMA
63,Dominican Republic (the),L_DOM
64,Ecuador,L_ECU
65,Egypt,L_EGY
66,El Salvador,L_SLV
67,Equatorial Guinea,L_GNQ
68,Eritrea,L_ERI
69,Estonia,L_EST
70,Eswatini,L_SWZ
71,Ethiopia,L_ETH
72,Falkland Islands (the) [Malvinas],L_FLK
73,Faroe Islands (the),L_FRO
74,Fiji,L_FJI
75,Finland,L_FIN
76,France,L_FRA
77,French Guiana,L_GUF
78,French Polynesia,L_PYF
79,French Southern Territories (the),L_ATF
80,Gabon,L_GAB
81,Gambia (the),L_GMB
82,Georgia,L_GEO
83,Germany,L_DEU
84,Ghana,L_GHA
85,Gibraltar,L_GIB
86,Greece,L_GRC
87,Greenland,L_GRL
88,Grenada,L_GRD
89,Guadeloupe,L_GLP
90,Guam,L_GUM
91,Guatemala,L_GTM
92,Guernsey,L_GGY
93,Guinea,L_GIN
94,Guinea-Bissau,L_GNB
95,Guyana,L_GUY
96,Haiti,L_HTI
97,Heard Island and McDonald Islands,L_HMD
98,Holy See (the),L_VAT
99,Honduras,L_HND
100,Hong Kong,L_HKG
101,Hungary,L_HUN
102,Iceland,L_ISL
103,India,L_IND
104,Indonesia,L_IDN
105,Iran (Islamic Republic of),L_IRN
106,Iraq,L_IRQ
107,Ireland,L_IRL
108,Isle of Man,L_IMN
109,Israel,L_ISR
110,Italy,L_ITA
111,Jamaica,L_JAM
112,Japan,L_JPN
113,Jersey,L_JEY
114,Jordan,L_JOR
115,Kazakhstan,L_KAZ
116,Kenya,L_KEN
117,Kiribati,L_KIR
118,Korea (the Democratic People's Republic of),L_PRK
119,Korea (the Republic of),L_KOR
120,Kuwait,L_KWT
121,Kyrgyzstan,L_KGZ
122,Lao People's Democratic Republic (the),L_LAO
123,Latvia,L_LVA
124,Lebanon,L_LBN
125,Lesotho,L_LSO
126,Liberia,L_LBR
127,Libya,L_LBY
128,Liechtenstein,L_LIE
129,Lithuania,L_LTU
130,Luxembourg,L_LUX
131,Macao,L_MAC
132,Madagascar,L_MDG
133,Malawi,L_MWI
134,Malaysia,L_MYS
135,Maldives,L_MDV
136,Mali,L_MLI
137,Malta,L_MLT
138,Marshall Islands (the),L_MHL
139,Martinique,L_MTQ
140,Mauritania,L_MRT
141,Mauritius,L_MUS
142,Mayotte,L_MYT
143,Mexico,L_MEX
144,Micronesia (Federated States of),L_FSM
145,Moldova (the Republic of),L_MDA
146,Monaco,L_MCO
147,Mongolia,L_MNG
148,Montenegro,L_MNE
149,Montserrat,L_MSR
150,Morocco,L_MAR
151,Mozambique,L_MOZ
152,Myanmar,L_MMR
153,Namibia,L_NAM
154,Nauru,L_NRU
155,Nepal,L_NPL
156,Netherlands (the),L_NLD
157,New Caledonia,L_NCL
158,New Zealand,L_NZL
159,Nicaragua,L_NIC
160,Niger (the),L_NER
161,Nigeria,L_NGA
162,Niue,L_NIU
163,Norfolk Island,L_NFK
164,Northern Mariana Islands (the),L_MNP
165,Norway,L_NOR
166,Oman,L_OMN
167,Pakistan,L_PAK
168,Palau,L_PLW
169,"Palestine, State of",L_PSE
170,Panama,L_PAN
171,Papua New Guinea,L_PNG
172,Paraguay,L_PRY
173,Peru,L_PER
174,Philippines (the),L_PHL
175,Pitcairn,L_PCN
176,Poland,L_POL
177,Portugal,L_PRT
178,Puerto Rico,L_PRI
179,Qatar,L_QAT
180,Republic of North Macedonia,L_MKD
181,Romania,L_ROU
182,Russian Federation (the),L_RUS
183,Rwanda,L_RWA
184,R�union,L_REU
185,Saint Barth�lemy,L_BLM
186,"Saint Helena, Ascension and Tristan da Cunha",L_SHN
187,Saint Kitts and Nevis,L_KNA
188,Saint Lucia,L_LCA
189,Saint Martin (French part),L_MAF
190,Saint Pierre and Miquelon,L_SPM
191,Saint Vincent and the Grenadines,L_VCT
192,Samoa,L_WSM
193,San Marino,L_SMR
194,Sao Tome and Principe,L_STP
195,Saudi Arabia,L_SAU
196,Senegal,L_SEN
197,Serbia,L_SRB
198,Seychelles,L_SYC
199,Sierra Leone,L_SLE
200,Singapore,L_SGP
201,Sint Maarten (Dutch part),L_SXM
202,Slovakia,L_SVK
203,Slovenia,L_SVN
204,Solomon Islands,L_SLB
205,Somalia,L_SOM
206,South Africa,L_ZAF
207,South Georgia and the South Sandwich Islands,L_SGS
208,South Sudan,L_SSD
209,Spain,L_ESP
210,Sri Lanka,L_LKA
211,Sudan (the),L_SDN
212,Suriname,L_SUR
213,Svalbard and Jan Mayen,L_SJM
214,Sweden,L_SWE
215,Switzerland,L_CHE
216,Syrian Arab Republic,L_SYR
217,Taiwan (Province of China),L_TWN
218,Tajikistan,L_TJK
219,"Tanzania, United Republic of",L_TZA
220,Thailand,L_THA
221,Timor-Leste,L_TLS
222,Togo,L_TGO
223,Tokelau,L_TKL
224,Tonga,L_TON
225,Trinidad and Tobago,L_TTO
226,Tunisia,L_TUN
227,Turkey,L_TUR
228,Turkmenistan,L_TKM
229,Turks and Caicos Islands (the),L_TCA
230,Tuvalu,L_TUV
231,Uganda,L_UGA
232,Ukraine,L_UKR
233,United Arab Emirates (the),L_ARE
234,United Kingdom of Great Britain and Northern Ireland (the),L_GBR
235,United States Minor Outlying Islands (the),L_UMI
236,United States of America (the),L_USA
237,Uruguay,L_URY
238,Uzbekistan,L_UZB
239,Vanuatu,L_VUT
240,Venezuela (Bolivarian Republic of),L_VEN
241,Viet Nam,L_VNM
242,Virgin Islands (British),L_VGB
243,Virgin Islands (U.S.),L_VIR
244,Wallis and Futuna,L_WLF
245,Western Sahara,L_ESH
246,Yemen,L_YEM
247,Zambia,L_ZMB
248,Zimbabwe,L_ZWE
249,�land Islands,L_ALA
